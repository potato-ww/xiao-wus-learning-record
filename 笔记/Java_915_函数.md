# 函数

跟数学无关，function功能块。

解决多余代码，利用函数，模块化，利于维护、团队协作等等。

## 语法

- public修饰符，定义方法访问范围。
- static静态的。
- void定义函数调用完有没有返回值。
- main主要的，主函数。
- （）参数，其实就是变量（a,b),参数让函数更灵活。

## 作用域

通过{}判断

## 3要素

功能、参数、返回值

参数：

- 形式参数：简称形参（parameter），是⽅法声明时，在⽅法名后的括号中声明的变 

量。

- 实际参数：简称实参（argument），是⽅法调⽤时，调⽤⽅提供的实际数据值。

例题：

```java
 public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入一个字符串和一个字符：");
        String str = sc.next();
        String cs = sc.next();
        int num = stringChar(str, cs);
        System.out.println(num);
    } 


/*【定义一个函数，接收一个 string 参数和一个 char，返回该字符在字符串中第一次出现的下标，如果没有返回 -1】*/
    public static int stringChar(String str, String cs) {
        char[] arr = str.toCharArray();
        boolean br = false;//判断是否有char值
        char c = cs.charAt(0);
        int num = 0;//接收下标
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == c) {
                br = true;
                return i;
            }
        }
        return -1;
    }
```

在写这个题的时候，一直在纠结char的值怎么接收，因为他不像其他的的有`.nextInt()`这样的方法,通过百度查下了一下char接收输入值，知道了可以通过String接收，再通过`char c = cs.charAt(0);`就可以完成char接收输入了,还有学到了函数遇到return就会结束。