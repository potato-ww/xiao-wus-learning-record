# Java_day_03_9_7

## `else if`

```java
		Scanner sc=new Scanner(System.in);
        System.out.println("请输入成绩：");
        int sum=sc.nextInt();
        if(sum>=90){
            System.out.println("你的等级为A");
        }else if(sum>=80){
            System.out.println("你的等级为B");
        }else if(sum>=60){
            System.out.println("你的等级为C");
        }else{
            System.out.println("你的等级为D");
        }
```

## `switch`

```java
			double money = 500;
			System.out.println("请选择您的操作：1、存钱 2、取钱 3、查询 4、退出");
            int handle = sc.nextInt();
            switch (handle) {
                case 1:
                    System.out.println("请输入您要存入的金额：");
                    double addMoney = sc.nextDouble();
                    money += addMoney;
                    System.out.println("您的余额：" + money);
                    break;
                case 2:
                    System.out.println("请输入您要取出的金额：");
                    double lestMoney = sc.nextDouble();
                    if (lestMoney <= money) {
                        money -= lestMoney;
                        System.out.println("取款成功！您的余额：" + money);
                    } else {
                        System.out.println("余额不足");
                    }
                    break;
                case 3:
                    System.out.println("您的余额：" + money);
                    break;
                case 4:
                    System.out.println("退出成功");
                    break;
                default:
                    System.out.println("错误操作！");
                    break;
            }
```



## 相等`==`and `equals()`

==值比较

String是引用类型用.equals（）比较

`name.equals(userName)`

## 模块化

if(true)才会执行

可以考虑把不同的功能写在不同的板块。