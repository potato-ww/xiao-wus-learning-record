## getter setter

get 获取  得到set

getter访问器，setter修改器

访问修饰符public

## 总结：

上午写了一个输入学生成绩并排序的作业，首先需要建立一个学生类，里面有姓名和成绩属性，由于是私有属性，所以要创建get set方法，还需要带参构造器；在测试类中，从题目中的5个同学，我们应该考虑用数组，刚开始我是建立了两个数组，一个存分数一个存姓名，但是在老师讲解以后意识到，那样只是一些零散的数据，所以正确的思路应该是建一个学生类的数组

`Students[] arr = new Students[num];//创建学生数组`，

然后再for里面输入姓名成绩，创建对应数量的实例对象，再将输入的姓名和成绩放入

`Students s = new Students(name, cj);//创建学生对象`

最后将这个学生放入学生数组`arr[i] = s;//成绩传入数组`，这样录入成绩就结束；排序和以前一样，需要注意的是比较的是学生的分数需要用`arr[i].getGrade()`调出学生分数，之后就和之前的一样了。

```java
	 public static void set(Students[] arr) {//排序
        for (int i = 0; i < arr.length; i++) {
            for (int j = i + 1; j < arr.length; j++) {
                if (arr[i].getGrade() < arr[j].getGrade()) {
                    Students num = new Students();//创建一个学生容器
                    num = arr[i];//将当前值存入容器
                    arr[i] = arr[j];
                    arr[j] = num;
                }
            }
        }
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i].getName() + " ");
        }
    }
```

下午写了一个数组扩容的题，首先要创建一个新数组`int[] arrAdd = new int[arr.length + 1];//新数组`，把旧数组的值传入新数组，然后把新加的数字放在数组末尾`arrAdd[arrAdd.length - 1] = num;//把加的数字存入数组`，最后排序。知道了数组长度创建好以后就是固定的，要加内容的话，就要写的这种，增加数量，称为扩容。

最后复习了一下类，函数，包，数组，复习了数组是引用类型的，数组的类型又分为引用类型数组、基本数据类型数组。

