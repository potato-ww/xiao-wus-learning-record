今天通过各种练习，大概理解了一下什么是功能？什么是模块化？函数是什么？

功能就是系统中的某个作用，函数就是来封装这一种功能的，模块化就是把一个大的模块，分割成各司其职的小模块，小模块就是一个里面只有某一种功能的函数。

排序：注意，双重for循环，第二个for循环从第二个开始，`i+1`,长度就是数组的长度：

```java
 	for (int i = 0; i < arr.length; i++) {
            for (int j = i + 1; j < arr.length; j++) {
               if (arr[i] > arr[j]) {//升
                        int num = arr[i];
                        arr[i] = arr[j];
                        arr[j] = num;
                    }
            }
        }
```

双色球，真是水很深，规则想了很久，然后就是封装功能的时侯欠考虑，首先，要明确要生成两个随机不重复6位1-33的数组，生成这样数组的功能函数可以写一个，然后调用两次就欧克了。篮球随机数一样的道理。然后就是按规则比较判断是否中奖，判断功能再封装成一个函数。调用传值就欧克了。代码：

```java
/*
    红球 6个 1-33  不重复
    蓝球 1个 1-16

    中奖规则：R + B  即 红球选中个数 搭配 蓝球号码。一等奖：6+1 即：红球中6个，蓝球也中

    一等奖：6+1              （钱(ni)太(zhong)多(bu)了，也封顶¥500万?）
    二等奖：6+0              （封顶¥500万）
    三等奖：5+1              （¥3000）
    四等奖：5+0 ｜ 4+1        （¥200）
    五等奖：4+0 ｜ 3+1        （¥10）
    六等奖：2+1 ｜ 1+1 ｜ 0+1 （¥5）

    中奖号码 6+1  [1,2,3,4,5,6]
    机选号码 6+1  [1,7,8,9,12,23]
*/
    public static void main(String[] args) {
        int[] machineNum = machine();//机选红
        int[] winNum = machine();//中奖红
        int blue = blueRandom();//机选蓝
        int winBlue = blueRandom();//中奖蓝
        compare(machineNum, blue, winNum, winBlue);
    }

    public static int blueRandom() {
        return (int) (Math.random() * 16 + 1);//随机蓝;
    }

    public static int[] machine() {//随机红
        int[] winNum = new int[6];
        for (int i = 0; i < winNum.length; i++) {
            boolean br = false;
            int num = (int) (Math.random() * 33 + 1);
            for (int j = 0; j < i; j++) {
                if (num == winNum[j]) {
                    br = true;
                }
            }
            if (br) {
                i--;
            } else {
                winNum[i] = num;
            }
        }
        return winNum;
    }

    public static void compare(int[] machine, int blue, int[] winNum, int winBlue) {
        int redNum = 0;//红球相等个数
        int blueNum = 0;//蓝球是否相等
        for (int i = 0; i < winNum.length; i++) {
            for (int j = 0; j < machine.length; j++) {
                if (machine == winNum) {
                    redNum++;//判断红色相等个数
                }
            }
        }
        if (winBlue == blue) {
            blueNum++;//判断蓝色相等
        }
        if (redNum == 6 && blueNum == 1) {
            System.out.println("一等奖");
        } else if (redNum == 6 && blueNum == 0) {
            System.out.println("二等奖");
        } else if (redNum == 5 && blueNum == 1) {
            System.out.println("三等奖");
        } else if ((redNum == 5 && blueNum == 0) || (redNum == 4 && blueNum == 1)) {
            System.out.println("四等奖");
        } else if ((redNum == 4 && blueNum == 0) || (redNum == 3 && blueNum == 1)) {
            System.out.println("五等奖");
        } else if ((redNum == 2 && blueNum == 1) || (redNum == 1 && blueNum == 1) || (redNum == 0 && blueNum == 1)) {
            System.out.println("六等奖");
        } else {
            System.out.println("未中奖！");
        }
    }
```

