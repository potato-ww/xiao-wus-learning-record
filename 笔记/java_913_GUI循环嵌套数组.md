## 循环嵌套:

格式：

```java
	for (int i = 1; i <=3; i++) {//行数
            for (int j = 1; j < 5; j++) {//列数
               System.out.print(j + " ");
            }
        System.out.println();
        }
输出：
    1 2 3 4 5
    1 2 3 4 5
    1 2 3 4 5
```

例题：

```java
		//打印出1-100所有的质数
	for (int i = 2; i <= 100; i++) {
            int num = 0;
            for (int j = 1; j < i; j++) {
                if (i % j == 0) {
                    num++;
                }
            }
            if (num < 2) {
                System.out.print(i + " ");
            }
        }*/
```





## GUI

`JOptionPane.showMessageDialog(null,content)`显示框

`String content=JOptionPane.showInputDialog(null,"请输入")`输入框

`int result=JOptionPane.showConfirmDialog(null,"确定吗")`确认框

字符串转整数：`Integer.parseInt("100")`

字符串转double：`Double.parsedoublet("100")`

# 数组

## 一维数组

格式：

```java
int[] arr=new int[10];//第一种
int[] arr2={1,2,3,4,5};//第二种
```

总结：

1. 下标都是从0开始
2. 数组长度`arr.length`,数组里面元素的个数
3. 只能存放同⼀种类型的数据
4. 所有元素数据存放在连续的内存空间中
5. 数组⼤⼩⼀旦确定，就不能更改；
6. 访问数组元素时，要特别注意不要越界

## 多维数组

格式：

```java
 		int[][] allArr = new int[4][5];
        int k = 0;
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 5; j++) {
                allArr[i][j] = k;
                k++;
                System.out.print(allArr[i][j] + " ");
            }
            System.out.println("");
        }
```

例题：

```java
/*已知5位同学的考试成绩，请找出最⾼分、最低分。*/

      Scanner sc = new Scanner(System.in);
        double[] cj = new double[5];
        System.out.println("请输入五位同学的成绩：");
        double max = 0;//最高分
        for (int i = 0; i < cj.length; i++) {
            cj[i] = sc.nextDouble();
            if (cj[i] > max) {
                max = cj[i];
            }
        }
        System.out.println("最高分为" + max);
```

```java
	/*【String[] users = {"zhangsan", "lisi"};
   	String[] passwords = {"123", "456"};】
   	实现登录，张三登录就欢迎张三，李四登录就欢迎李四*/
        String[] users = {"zhangsan", "lisi"};
        String[] passwords = {"123", "456"};
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入您的用户名和密码");
        String username = sc.next();
        String password = sc.next();
        boolean flg = true;
        for (int i = 0; i < users.length; i++) {
            if (users[i].equals(username) && passwords[i].equals(password)) {
                System.out.println(users[i] + "欢迎您！");
                flg = false;
                break;
            }
        }
        if (flg) {
            System.out.println("输入有误");
        }
```

区分：

- .print（）方法
- .length 属性

总结：今天我们学习了循环嵌套、GUI、数组。在循环嵌套练习中发现，break、continue只作用于本层循环，最外层的相当于行，里层相当于列，编写双层循环最好打个草稿，逻辑会更清晰。在使用了GUI后发现，界面变的更加美观了，他代替了scanner，需要注意的是他的输入框返回的是String类型，根据需要转换类型，确实框的返回值是int类型，确认是0，否是1，取消是2。在数组的练习中发现，一定要注意数组的下标，不然会造成数组下标超界的错误，写多维数组题的时候，和循环嵌套一样打草稿，不然逻辑容易混乱；

