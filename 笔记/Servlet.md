# http协议：

HTTP协议称为超文本传输协议，是客户端和服务器一种通信的标准，客户端和服务器都遵循http协议，就能从彼此发送的信息中提取自己想要的数据。

## http协议的特点：

1. http协议遵循的是请求/响应模型。

2. http是一种无状态的协议。当请求响应完成以后会断开，对服务器而言，会认为每次请求的都是新用户。

3. http协议占用80端口，短连接。

   Http/1.1版本用的是长连接。长连接意味着，在请求/响应完成以后，连接并不会马上离开。而是保留一段时间，在这段时间内，客户端再次请求服务器时，就不会在新建连接了，比如：客户端访问一个网页，如果网页有链接资源的时候，客户端下载链接资源的第二次第三次请求，不会再创建新的连接，从而提高响应速度。

## http处理流程

1. 客户端和服务器建立连接
2. 客户端向服务器发送HTTP请求
3. 服务器根据客户端请求的数据，产生响应
4. 关闭连接

## HTTP协议的规范

1. 请求行：描述请求方式、请求url路径、协议版本
2. 请求头：以键值对方式，描述客户端相关信息
3. 空行：标识请求头的结束
4. 消息体（post）：描述客户端请求的表单数据

## HTTP协议响应信息

1. 状态行：描述http协议的版本，状态码，描述状态。
2. 响应头：以键值对方式描述服务器相关信息。
3. 空行：标识响应头结束。
4. 消息体：服务器发送给客户端的具体数据。

## web服务器：产生响应信息时，只能产生静态HTML页面

## web容器：是一种辅助应用，在接收到请求以后，会将请求信息进行一系列处理，动态产生响应信息。

## web应用服务器：web服务器+web容器 合称web应用服务器

## web服务器的缺陷

web服务器只能给客户端提供静态网页内容。影响响应信息的灵活性。

解决方案：在web服务器之后添加辅助应用，在web服务器接收到客户端的请求以后，由辅助应用处理客户端的请求的数据，然后，用数据库作为介质，动态参生响应信息。

java利用web容器+servlet作为作为辅助应用解决方案。



在java中，web容器完成底层操作，预留servlet接口，交给开发者书写业务操作，web容器调用servlet业务方法，动态参生响应信息。

### web容器的作用

1. 通讯支持。

   web容器需要将请求/响应信息封装成请求对象和响应对象，以便更好的发送和接收数据

2. Servlet组件的生命周期管理

   servlet是web容器预留给开发者书写业务方法的接口，开发者完成的业务组件由web容器负责参生，销毁，方法调用。

3. 多线程的支持

   为了做到客户端能同时访问服务器，服务器通过多线程做多任务处理方式，线程的创建，运行，同步销毁都是由web容器完成。

4. JSP支持

   负责jsp引擎的实现，可以将jsp页面翻译成java类。

5. 安全性处理

# Servlet

## servlet规范

java利用web容器+servlet作为作为辅助应用，各个厂商在完成具体的实现时，各有各的标准，这样对于不同容器厂商，就会有API、web应用程序结构、以及性能的差异。同一个web应用程序，也就不能在不同厂商的web容器运行。

为了规范不同厂家之间的差异，sun公司制定了servlet规范，该规范定义了一系列的标准和一系列接口，由不同的厂商提供实现类，开发者根据接口调用方法，就可以屏蔽不同厂家的差异，web应用程序也就有了移植性。

## web应用程序的组成

按照servlet规范，web应用程序中，必须提供web-inf文件夹，该文件夹不能别客户端直接访问，用于存放比较隐秘的信息。

### web应用程序名称

\*.html 	\*.jpg	\*.css	\*.js	 //可以直接访问的

//不能直接访问的

web-inf //存放隐秘信息

​		classes //存放开发者编译以后的class文件

​		lib //存放第三方的架包

​		web.xml //用于web组件的注册

## tomcat

tomcat源自于Apache软件基金会Jakarta项目。Tomcat是一款免费的、开源的、性能稳定的、技术成熟的web容器产品。

1、下载tomcat镜像

Docker安装Tomcat：docker pull tomcat:10

2、进入容器

docker exec -it tomcat10 /bin/bash

![image-20230202152911246](C:\Users\60952\AppData\Roaming\Typora\typora-user-images\image-20230202152911246.png)

webapps为tomcat存放web应用程序的目录。但目录在dockers容器中，管理比较麻烦，所有要映射到本地目录中便于维护。

3、创建tomcat容器

docker run --name tomcat10 -d -p 8088:8080 -v H:/Springxx/PhaseII/webProject:/usr/local/tomcat/webapps  tomcat:10 

4、创建web项目步骤

创建新模块，选择![image-20230202152608799](C:\Users\60952\AppData\Roaming\Typora\typora-user-images\image-20230202152608799.png)

补齐目录

![image-20230202152646421](C:\Users\60952\AppData\Roaming\Typora\typora-user-images\image-20230202152646421.png)

Servlet是运行在web服务器或应用服务器上的Java程序，是web容器和开发者业务组件通信的标准。由sun规范了其功能。

## servlet的API结构

在servlet规范中，提供了业务接口：Servlet,Servlet接口中的service方法为提供给开发者书写业务操作的业务方法。

另外，还提供了配置接口：ServletConfig。用于读取servlet配置信息

GennericServlet同时实现了Servlet和ServletConfig两个接口，并实现了大部分方法，但是没有实现service（）方法，该方法应该由GenericServlet的子类，根据子类协议的特点分别实现。

HttpServlet是GenericServlet 的子类，该类根据HTTP协议的特点，重写了service（）方法，在service()中根据请求的不同分别去调用doXXX()。

开发者如果使用HTTP协议，只需要继承HttpServlet,重写doGet和doPost()就可以了。

## servlet开发流程

1. 导入依赖

   ```
   <dependency>
       <groupId>jakarta.servlet</groupId>
       <artifactId>jakarta.servlet-api</artifactId>
       <version>5.0.0</version>
       <scope>provided</scope>
   </dependency>
   ```

2. 导入打包插件

   ```
   <plugin>
       <artifactId>maven-war-plugin</artifactId>
       <version>3.2.2</version>
   </plugin>
   ```

3. 编写servlet

   ```
   public class TestServlet extends HttpServlet {
       @Override
       protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
           //向客户端输出文本数据
           resp.getWriter().println("hello vv");
       }
   
       @Override
       protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
           this.doPost(req,resp);
       }
   }
   ```

4. 完成servlet注册

   方法一：在WEB-INF/web.xml中编写

   ```xml
   <servlet>
       <!-- //servlet名称-->
       <servlet-name>test</servlet-name> 
       <!-- //servlet类-->
       <servlet-class>com.vv.servlet.TestServlet</servlet-class>
    </servlet>
    <servlet-mapping>
       <servlet-name>test</servlet-name>
      <!-- 客户端访问路径-->
       <url-pattern>/ts</url-pattern>
    </servlet-mapping>
   ```
   方法二：通过注解，servlect3以后支持

   ```java 
   @WebServlet("/first")
   public class FirstServlet extends HttpServlet {
       @Override
       protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
           this.doPost(req,resp);
       }
   
       @Override
       protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
           //请求编码集
           req.setCharacterEncoding("utf-8");
           //响应编码集
           //解决中文乱码，设置响应信息的MIME类型和编码集
          resp.setContentType("text/html;charset=utf-8");
          resp.getWriter().println("hello bb");
       }
   }
   ```

5. 打包放入tomcat/webapps中

   web应用程序需要部署到web容器中，才能得到web容器提供的服务，通常会先将web应用程序打包成war包，再部署到web容器中。

6. 打开浏览器输入：

   http://localhost:8088/webTest/ts就可以访问servlet。

   http://localhost:8088/webTest/first  注解

### MIME

由服务器产生响应信息时，告诉客户发送的是什么数据类型，客户端会根据不同的mime类型来决定该如何解析响应信息

常见类型：

text/html   html网页格式数据

text/xml xml格式数据

image/jpeg jpg图片格式

application/json json格式数据

## 客户请求url路径问题

当表单提交时，会在action属性中指定访问服务器资源的url路径。

1. 在action属性中，直接书写url路径，表示当前目录下访问

   如果当前目录为HTML。那么访问路径为/html/first

   ```html
   <form action="first" method="post">
       <input type="text" name="name">
       <input type="submit" value="提交">
   </form>
   ```

2. 在action属性中以/开始书写url路径，表示返回到web apps的根目录，这时要访问某个web应用程序时，所以需要加上web应用的程序名称。

```html
<form action="/webTest/first" method="post">
    <input type="text" name="name">
    <input type="submit" value="提交">
</form>
```

## servlet的url路径

1. 精确映射   /first  客户端只能以/first路径才能访问servlet
2. 扩展映射   *.do 客户端以.do结尾的请求都能访问servlet
3. 路径映射 /aaa/* 客户端以/aaa这个目录开始的请求都能访问servlet

## Servlet初始化信息

在配置文件里面配置一些初始化信息

```xml
<servlet>
    <servlet-name>test</servlet-name>
    <servlet-class>com.vv.servlet.TestServlet</servlet-class>
     <!--初始化值-->
    <init-param>
        <param-name>name</param-name>
        <param-value>tiger</param-value>
    </init-param>
</servlet>
```

在servlet中，可以根据初始化参数名称，得到初始化值

```html
//获取配置对象
ServletConfig config = this.getServletConfig();
//根据初始化参数的名称获取值
String value = config.getInitParameter("name");
//向客户端输出文本数据
resp.getWriter().println("hello vv----" + value);
```

## servlet生命周期

servlet的生命周期是指，servlet对象的产生到销毁的整个过程。

1. web容器加载并实例化servlet。如果配置servlet时，加上了

   ```xml
   <!--  加载时间-->
     <load-on-startup>1</load-on-startup>
   
   <!-- 并且为0或正数，表示容器启动就实例化servlet，如果没配置或为负数，表示第一次访问servlet的时候完成servlet的实例化-->
   ```

2. 调用init（）完成初始化

3. 当请求到达时，调用service（），接收请求，产生响应。

4. 销毁阶段，调用destory(),完成资源清理。

在整个生命周期中，1、2、4都只进行一次，只有3每次请求到达都会执行，而servlet是单实例多线程的对象。

## 得到表单数据

```java
//得到表单数据
String name = req.getParameter("name");
//根据表单名，得到提交数据数组（多个同名键值对，复选框）
String[] arr = req.getParameterValues("like");
//如果有多个同名值只会返回第一个的值
String like = req.getParameter("like");
```

## 服务器发送数据给客户端发送数据的格式有两种：文本数据和二进制数据

```java
//向客户端发送文本数据
Writer w=resp.getWriter();
//向客户端发送二进制数据
OutPutStream out=resp.getOutPutStream();
```

## http协议请求方式

GET    POST   DELETE   HEAD   OPTIONS   PUT  TRACE    CONNECT

get      post     delete    head     options      put   trace       connect

其中最常用的是：GET  POST

GET请求：

1. 在浏览器中输入URL路径
2. 点击超链接
3. `<form method="get">`或不写

POST请求：`<form method="post">`

### GET请求和POST请求的区别：

1. 流格式的区别

   GET请求只有请求行、请求头、空行。表单数据加在URL后面。POST请求有请求行、请求头、空行、消息体。消息体中附近表单数据。

2. 用途的区别

   GET请求用于资源的查找，POST请求用于数据的传输。

3. 传输性能的区别

   GET请求只能传输小文本数据，在表单提交时会在浏览器地址栏中显示表单数据。

   POST可以传输文本数据，也可以传输二进制数据，而且传输数据的大小没有限制。表单提交时不会在地址栏中显示。

### POST和GET的选用

1. 请求一个静态页面或图形使用GET
2. 发送大数据时用POST
3. 表单提交私密数据用POST
4. 上传文件时用POST

## HttpServletRequest常用方法

```Java
//得到表单数据
String name = req.getParameter("name");
//根据表单名，得到提交数据数组（多个同名情况，复选框）
String[] arr = req.getParameterValues("like");
//如果有多个同名值只会返回第一个的值
String like = req.getParameter("like");
//得到请求方式
String method = req.getMethod();
//获取url后面的表单数据，只对get有效，因为post后面没有表单数据
String quary = req.getQueryString();
//得到url请求路径
String url = req.getRequestURL().toString();
//根据请求头的名称或去请求头的值
String accept = req.getHeader("Accept");
```

## HTTP状态码

100-199：表示信息性代码，表示客户端应该采取其他动作，请求正在进行

200-299：表示客户请求成功

300-399：表示用于已经移走的资源文件，指示新地址

400-499：表示由客户端引起的错误

500-599：表示服务器端引发的错误

## HttpServletRespnose常见方法

```Java
//设置状态码和响应描述
resp.sendError(404, "田熹是pig");
//设置响应头
resp.setHeader("name","熹熹子");
```

## 查找mysql服务器ip地址

查看mysql的：docker ps

进入mysql：docker exec -it f125 /bin/bash

进入etc:cd etc

查看地址：cat hosts

## 业务跳转

在servlet中，有时候要在完成业务以后，转发到另一个servlet或页面中，显示结果，转发的方式有两种，重定向、请求转发。

重定向的工作流程：客户端访问服务器，服务器调用response.sendRedirect()完成重定向。产生响应时，会产生302状态码和location响应头，在location响应头中附加重定向路径。客户端在接收到响应以后，发现是302状态码，就会读取location响应头的信息，根据这个信息，重新向服务器发出第二次请求。

```java
resp.sendRedirect("/userTest/findAll");
```

请求转发：客户端访问服务器后，如果希望访问别的资源 的时候，在服务器内部进行资源调配。这时客户端只发生一次请求，效率较高。可以得到request中的绑定的共享数据。

```java
req.getRequestDispatcher("/findAll").forward(req, resp);
```

在request请求对象中，可以使用setAttribute（）和getAttribute，绑定数据在一次请求范围内共享数据，由于第二次请求时，会产生新的request对象，所以，无法共享之前的request对象中绑定的共享数据。

```java
//绑定共享数据
req.setAttribute("age", 123);
```

```java
//获取共享数据
Object o=req.getAttribute("age");
```

## 重定向和请求转发的区别

1. 重定向是response发出，而请求转发由request发出。
2. 重定向客户端会发出两次请求，而请求转发客户端只发出一次请求。所以，请求转发效率高，同时，由于请求转发客户端只发出一次请求，所以可以共享request中绑定的共享数据，而定向不行。
3. 请求转发地址栏不会发生变化，而重新定向浏览器地址栏会定位到真实的url路径。
4. 请求转发只能转发服务器内部的资源，而重定向可以在第二次请求时，定位到别的服务器。

# SpringMVC

spring MVC是一个基于MVC模式的表现层框架，在spring2.5以后新增加了注解功能，和同类的MVC表现层框架相比，有如下特点：

1. 基于servlet框架
2. 控制器不在需要继承其他类
3. 应用控制器方法参数，由前端控制器负责封装，方法签名定义灵活
4. 返回页面直接在框架中指定，可以是String，也可以是其他的，比如：ModelAndView或void等。
5. 性能也很优秀。

## springMVC有两种控制器：前端控制器，应用控制器

前端控制器（DispatcherServlet）:负责接收客户端请求，根据请求路径访问应用控制器，负责将页面参数填充javabean，负责转发页面，对标签类进行调用。

应用控制器（用户书写的Controller）,负责产生业务组件，调用业务组件的方法完成业务，根据结果返回转发的页面对象。

## Springmvc工作流程

1. 当客户端请求服务器，服务器使用前端控制器DispatcherServlet接收请求
2. DispacherServlet借助HandlerMapper,根据请求路径，定位到具体的Controller,和应用控制器的具体方法，并将封装好数据的实体对象传入应用控制器方法
3. 由应用控制器方法，完成业务组件的业务方法的调用，然后根据业务方法处理的结果，返回需要转发的页面路径，DispatcherServlet根据路径，完成页面转发。

## springboot

springboot 是由Pivotal团队提供的全新框架，其设计的目的是用来简化Spring应用的初始搭建以及开发过程。

## springboot环境搭建

1. 在父工程中导入依赖

   ```xml
   <parent>
       <groupId>org.springframework.boot</groupId>
       <artifactId>spring-boot-starter-parent</artifactId>
       <version>2.6.13</version>
       <relativePath/>
   </parent>
   
   ```

2. 导入启动器依赖

   springboot是由一系列启动器组成的，这些启动器构成了一个强大的灵活的开发助手，开发人员根据项目需求，选择并组合相应的启动器，就可以搭建一个适合的项目需要的基础运行框架。

   ```xml
   <dependencies>
       <dependency>
           <groupId>org.springframework.boot</groupId>
           <artifactId>spring-boot-starter-web</artifactId>
       </dependency>
   </dependencies>
   ```

3. 创建启动类

   所以的spring组件都必须在启动类的包或者子包中。

   ```Java
   @SpringBootApplication
   public class MainBoot1 {
       public static void main(String[] args) {
           SpringApplication.run(MainBoot1.class);
       }
   }
   ```

4. 创建应用控制器

   url路径/可加可不加

   ```Java
   @RestController
   public class TestController {
       @RequestMapping("xx")
       public String speak(){
           return "hello 嘻嘻";
       }
       @RequestMapping("/vv")
       public String test(){
           return "my name is vv";
       }
   }
   ```

5. 执行启动类，开启tomcat，端口默认为8080，打开浏览器输入：http://localhost:8080/vv就可以看到my name is vv。

## SpringBoot的配置文件

springboot有很多的自动配置，在开发中，我们可能需要修改Springboot自动配置的默认值。比如服务器启动端口，数据源等。

springboot的配置文件，application.properties或application.yml

application.properties:

server.port=8088



application.yml:

server:

​	port:8088

## springboot静态资源存优先级

Springboot静态资源存放目录，需要在classPath类路径下，如果是maven项目，则在resource目录下。

META-INF/resources>resources>static>public

静态资源用以上4个目录其中一个，如果有多个，按照优先级。

## springBood得到表单数据

```Java
@RequestMapping("/add")
public String add(UserBean user,String phone,@RequestParam("userAge") String age){
    return "用户信息"+user+phone+age;
}
```

springboot得到表单数据，直接在控制器方法中添加形参即可，注意，如果形参封装表单数据时，形参名和表单名要相同，如果形参名与表单名不同，需要加上@RequestMapping("name")说明什么表单名对应的表单值，充当当前形参。

如果控制器方法中，形参为实体对象，按照表单名和属性名相同的原则，完成数据填充。

### 自定义类型转换器

springboot 中有很多默认的类型转换器，如果需要自定义类型转换，则需要实现Converter接口，重写convert方法，转换器类需要放在启动类子包中。

```Java
@Component
public class LocalDateChange implements Converter<String, LocalDate> {

    @Override
    public LocalDate convert(String source) {
        if(source!=null&&source.matches("(19|20)\\d{2}-\\d{2}-\\d{2}")){
            return LocalDate.parse(source);
        }
        return null;
    }
}
```

## url路径重名

在开发时，应用控制器会有很多方法需要接收客户端的请求，每个方法都会有一个访问url路径，当业务较多时，很容易造成url路径同名，从而导致springboot启动失败，为了防止路径同名，可以使用命名空间。

```java
@RestController
@RequestMapping("/books")
public class BookController {
    @Autowired
    private BookService bookService;
	@RequestMapping("/add")
    public String add(UserBean user,String phone,@RequestParam("userAge") String age){
        return "用户信息:"+user+"电话："+phone+"年龄："+age;
    }
}
```
## 应用控制器的访问方式

在定义应用控制器方法的时候可以指定只能用哪种请求方式访问。

```java
//表示该方法只能用post请求访问，不然就会抛405错误。
@RequestMapping(value = "xx",method = RequestMethod.POST)
或者
@PostMapping("xx")
    
@RequestMapping(value = "xx",method = RequestMethod.GET)
或者
@GetMapping("xx")
```

```

```

## @PathVariable

在rest风格中，很多时候会将数据存放在url路径中，在应用控制器方法中，可以设置url的占位符，通过PathVariable（“name"）根据占位符名称，得到占位符内容。

```java
@RequestMapping("del/{name}/{id}")
public String del(@PathVariable("name") String name,@PathVariable("id") Integer id){
    return "删除<br>name="+name+"<br> id="+id;
}
```

http://localhost:8082/user/del/维维/1

会将维维赋值给name，1赋值给id

## 设置默认值

```java
@RequestMapping("cutAll")
public String find(@RequestParam(value = "pageNO" ,defaultValue = "1") Integer pageNO){
    return "page="+pageNO;
}
```

如果是http://localhost:8082/user/cutAll那么pageNO=1

如果是http://localhost:8082/user/cutAll?pageNO=3那么pageNO=3

## 在应用控制方法中，有时候也需要访问servlet规范的相关对象，比如，请求、响应对象，这时可以直接在应用控制器方法中，添加形参即可。springboot可以完成自动注入。

```java
@RequestMapping("mytest")
public void mytest(HttpServletRequest request, HttpServletResponse response) throws IOException {
    String id=request.getParameter("id");
    response.getWriter().println("id="+id);
}
```

## controller注解

如果在注册应用控制器时添加controller注解，在应用控制器方法中，返回的字符串，会以请求转发的方式到指定资源。

```java
@Controller
@RequestMapping("student")
public class StudentController {
    @RequestMapping("speak")
    public String speak(){
        System.out.println("speak");
        //return "/index.html";//默认，请求转发
        return "redirect:/index.html";//重定向
    }
}
```

如果在@controller注解的应用控制器而言，在控制器方法上加上@ResponseBody对于返回字符串，也做原样显示

@controller+@ResponseBody=@RestController

```java 
@Controller
@RequestMapping("student")
public class StudentController {
    @RequestMapping("speak")
    @ResponseBody
    public String speak(){
        System.out.println("speak");
        //return "/index.html";
        return "redirect:/index.html";

    }
}
```

访问时在页面会得到：redirect:/index.html

## @Controller和@RestController的区别

@controller注册的应用控制器方法中，如果返回的是字符串，默认会以请求转发到指定页在，而@RestController会 将字符串原样发送到客户端。

如果返回是对象或集合，@RestController会将对象或集合转换成json字符串输出到客户端，@Controller需要加上@ResponseBody,才能将对象或集合转化为json字符串。

## springboot热部署

在开发阶段,需要频繁的测试，以及 BUG的修复。如果每次进行做小的改动都需要重启服务器，开发效率显得很低。热布署的含义是，在不重启服务器的情况下，让修改的代码及时生效。
开发阶段，可以通过SpringBoot提供的开发者工具spring-boot-devtools，来实现热部署。

1. 导入依赖

   ```xml
   <dependency>
       <groupId>org.springframework.boot</groupId>
       <artifactId>spring-boot-devtools</artifactId>
   </dependency>
   ```

2. ![img](https://static.dingtalk.com/media/lQLPJxvn7cwcbOjNATjNAhKwRXs7STP9OFcD3EB9ywDKAA_530_312.png_720x720q90g.jpg?bizType=im)

```
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-devtools</artifactId>
</dependency>
```

2018版，其他版本可以在高级设置中勾选

然后![image-20230207143245562](C:\Users\60952\AppData\Roaming\Typora\typora-user-images\image-20230207143245562.png)

验证码是一种区分用户是计算机还是人的公共全自动程序，可以防止：恶意破解密码、刷票、论坛灌水，有效防止某个黑客对某一个特定注册用户特定程序暴力破解方式进行不断登录尝试。

# mybatis-plus

简称 MP）是一个 [MyBatis (opens new window)](https://www.mybatis.org/mybatis-3/)的增强工具，在 MyBatis 的基础上只做增强不做改变，为简化开发、提高效率而生。

mybatis-plus提供了强大的CRUD操作，内置通用Mapper、通用Service,仅仅通过少量配置实现单表大部分CRUD操作，

## 环境搭建

1. 在父工程导入依赖，导入mybatis-plus依赖

   ```xml
   <parent>
       <groupId>org.springframework.boot</groupId>
       <artifactId>spring-boot-starter-parent</artifactId>
       <version>2.6.13</version>
       <relativePath/>
   </parent>
   <dependencies>
       <dependency>
           <groupId>mysql</groupId>
           <artifactId>mysql-connector-java</artifactId>
           <version>5.1.49</version>
       </dependency>
   
       <dependency>
           <groupId>org.springframework.boot</groupId>
           <artifactId>spring-boot-starter</artifactId>
       </dependency>
       <dependency>
           <groupId>org.springframework.boot</groupId>
           <artifactId>spring-boot-starter-test</artifactId>
           <scope>test</scope>
       </dependency>
       <dependency>
           <groupId>com.baomidou</groupId>
           <artifactId>mybatis-plus-boot-starter</artifactId>
           <version>3.5.3.1</version>
       </dependency>
       <dependency>
           <groupId>com.alibaba</groupId>
           <artifactId>druid</artifactId>
           <version>1.2.9</version>
       </dependency>
       <dependency>
           <groupId>org.junit.jupiter</groupId>
           <artifactId>junit-jupiter-api</artifactId>
           <version>5.9.0</version>
           <scope>test</scope>
       </dependency>
   </dependencies>
   ```

2. 在resource里面创建yml配置文件

   ```yml
   spring:
     datasource:
       driver-class-name: com.mysql.jdbc.Driver #驱动
       username: root #用户名
       password: txtx #密码
       url: jdbc:mysql://localhost:12345/project131?characterEncoding=utf8&allowMultiQueries=true #url
       type: com.alibaba.druid.pool.DruidDataSource #连接池数据源
       druid:
         one:
           max-active: 100 #最大连接
           min-idle: 20 #最小连接
           max-wait: 2000 #超时时间
   
   mybatis-plus:
     configuration:
       log-impl: org.apache.ibatis.logging.stdout.StdOutImpl #日志
     type-aliases-package: com.vv.bean #允许使用指定包中的实体类类名作为别名
     mapper-locations: classpath:*/*Mapper.xml #扫描mapper文件
   ```

3. 创建实体类

   ```java
   @TableName("t_user")
   public class UserBean {
       //配置主键列，value为该属性的映射列表
       //type表示主键生成策略，IdType.AUTO表示利用数据库表自动增长作为主键生成方式
       @TableId(value = "pk_userId",type = IdType.AUTO)
       private Integer id;
       @TableField("u_username")
       private String userName;
       @TableField("u_pwd")
       private String pwd;
       @TableField("u_birthday")
       private LocalDate birthday;
       @TableField("u_money")
       private int money;
   }
   ```

4. 创建业务接口

5. 创建mapper接口

   ```java
   @Mapper
   public interface IUserMapper extends BaseMapper<UserBean> {
   }
   ```

6. 创建业务接口的实现类

   ```java
   @Service
   @Transactional
   public class UserServiceImpl implements IUserService {
       @Autowired
       private IUserMapper mapper;
   
       @Override
       public void add(UserBean user) {
           mapper.insert(user);
       }
   
       @Override
       public void del(Integer id) {
           mapper.deleteById(id);
       }
   
       @Override
       public void update(Integer id, Integer money) {
           UserBean userBean = mapper.selectById(id);
           userBean.setMoney(money);
           mapper.updateById(userBean);
       }
   
       @Override
       public UserBean findById(Integer id) {
           return mapper.selectById(id);
       }
   
       @Override
       public List<UserBean> findAll() {
           return mapper.selectList(null);
       }
   
       @Override
       public List<UserBean> findByBirthday(LocalDate starDate, LocalDate endDate) {
           QueryWrapper<UserBean> qw = new QueryWrapper<>();
           //大于等于
           qw.ge("u_birthday", starDate);
           //小于等于
           qw.le("u_birthday", endDate);
           return mapper.selectList(qw);
       }
   
       @Override
       public List<UserBean> findByName(String name) {
           QueryWrapper<UserBean> qw = new QueryWrapper<>();
           qw.like("u_username", name);
           return mapper.selectList(qw);
       }
   }
   ```

7. 创建启动类

   ```java
   @SpringBootApplication
   public class MainPlus {
       public static void main(String[] args) {
           SpringApplication.run(MainPlus.class);
       }
   }
   ```

8. 创建测试类

   ```
   @SpringBootTest(classes = MainPlus.class)//扫描启动类包及其子包的内容
   public class UserTest {
       @Autowired
       private IUserService service;
   
       @Test
       public void test() {
       }
   }
   ```

## 应用控制器向客户端输出json数据。

应用控制器方法返回实体对象时，或以集合方式时，会以json字符串格式发送到客户端时。

如果应用控制器方法，返回是map集合，发放到客户端的是json对象。

## 实体类注解解释

```java
@TableName("t_user")
public class UserBean {
    //配置主键列，value为该属性的映射列表
    //type表示主键生成策略，IdType.AUTO表示利用数据库表自动增长作为主键生成方式
    @TableId(value = "pk_userId", type = IdType.AUTO)
    private Integer id;
    //@TableField("u_username")表示属性对应的数据库表列
    //@TableField这样写也会映射数据库的列，与属性同名的列
    @TableField("u_username")
    private String userName;
    @TableField("u_birthday")
    private LocalDate birthday;
    //说明这个属性不对应数据库的列
    @TableField(exist = false)
    private Integer age;
```

## 计算两个日期直接的天数

```java
ChronoUnit.YEARS.between(第一个日期，第二个日期) 返回是long类型
```

## 动态分页查询

1. 添加分页插件

   ```java
   @Configuration
   public class Config {
       /**
        * 注册分页插件
        *
        * @return
        */
       @Bean
       public MybatisPlusInterceptor mybatisPlusInterceptor() {
           MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();
           interceptor.addInnerInterceptor(new PaginationInnerInterceptor(DbType.MYSQL));
           return interceptor;
       }
   }
   ```

2. 定义业务接口方法

   ```java
   public IPage<UserBean> cutByItem(Integer pageNO, String name, LocalDate starDate, LocalDate endDate);
   ```

3. 书写实现类

   ```java
   @Override
   public IPage<UserBean> cutByItem(Integer pageNO, String name, LocalDate starDate, LocalDate endDate) {
       QueryWrapper<UserBean> qw = new QueryWrapper<>();
       if (name != null && name.length() != 0) {
           qw.like("u_username", name);
       }
       if (starDate != null) {
           qw.ge("u_birthday", starDate);
       }
       if (starDate != null) {
           qw.le("u_birthday", endDate);
       }
       return mapper.selectPage(new Page<>(pageNO, 3), qw);
   }
   ```

4. 编写测试类

   ```java
   IPage<UserBean> page = service.cutByItem(1, "", LocalDate.parse("1999-01-01"), LocalDate.parse("2000-03-03"));
   System.out.println("满足条件的记录" +page.getRecords() + "总记录数" + page.getTotal() + "总页数" + page.getPages());
   ```

## 联表分页查询

1. 创建业务接口

   ```java
   public IPage<UserBean> cutByItem(Integer pageNO, String name, LocalDate starDate, LocalDate endDate);
   ```

2. 编写mapper方法

   ```java
   public IPage<StudentBean> cutByItem(Page page, @Param("name") String name, @Param("className") String className);
   ```

3. 编写mapper文件

   ```xml
   <select id="cutByItem" resultMap="stuMap">
       select *
       from t_student
       join t_class on fk_classId = pk_classId where 1=1
       <if test="name!=null and name!=''">
           and s_name like "%"#{name}"%"
       </if>
       <if test="className!=null and className!=''">
           and c_name like "%"#{className}"%"
       </if>
   </select>
   ```

4. 书写业务接口的实现方法

   ```java
   @Override
   public IPage<StudentBean> cutByItem(Integer pageNO, String name, String className) {
       return mapper.cutByItem(new Page(pageNO, 3), name, className);
   }
   ```

5. 编写mybatisplus插件配置文件

   ```java
   @Configuration
   public class Config {
       /**
        * 注册分页插件
        *
        * @return
        */
       @Bean
       public MybatisPlusInterceptor mybatisPlusInterceptor() {
           MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();
           interceptor.addInnerInterceptor(new PaginationInnerInterceptor(DbType.MYSQL));
           return interceptor;
       }
   }
   ```

6. 测试

   ```java
   System.out.println(studentService.cutByItem(1, "田", "小").getRecords());
   ```

# thymeleaf

## 应用控制器的缺陷

应用控制器对于生成动态网页繁琐，不利于项目分工。

解决方案有两种：服务器渲染页面，客户端渲染页面

服务器渲染页面由模板引擎来实现。DOM树在服务器端生成，然后返回给前端。

客户端渲染页面由Ajax实现。前端去后端取数据生成DOM树。

服务器渲染的优点：

1. 有利于搜索引擎的优化（SEO），因 为在后端有完整的html页面，所有爬虫更容易爬取信息
2. 有利于手机客户端APP临时数据的添加，比如：双11时，有大量临时降价商品需要展示，这时，客户端的APP不可能临时进行修改。那么，这时可以向客户端发送链接，从服务器渲染WEB页面。

服务器渲染的缺点：

1. 服务器渲染，对服务器压力比较大，可以使用服务器端的页面缓存技术，减轻服务器的缓存压力。
2. 不适合前后端分离开发，开发效率比较低。

客户端渲染的优点：

1. 前后端分离，开发效率高
2. 用户体验更好我们将网页做成SPA（单页面应用）或者部分页面做成SPA，当用户点击时，不会形成频繁的跳转。

客户端渲染的缺点：

1. 前端响应速度慢，特别是首屏
2. 不利于SEO优化，因为爬虫不认识SPA，所以他只能记录一个页面
3. 当客户端是手机APP时，无法适应临时新功能的添加。

## 模板引擎

模板引擎是在HTML网页基础上，以特殊的标记描述服务器动态数据的方式，模板引擎需要由服务器进行翻译，然后翻译后生成的静态HTML代码发给客户端。

### 常见的模板引擎：

thyme leaf

Velocity

Freemarker

jsp

### thyme leaf开发流程

1. 导入依赖

   ```xml
   <dependency>
       <groupId>org.springframework.boot</groupId>
       <artifactId>spring-boot-starter-thymeleaf</artifactId>
   </dependency>
   ```

2. yml文件添加配置

   ```xml
   thymeleaf:
     enabled: true  #开启thymeleaf视图解析
     encoding: utf-8  #编码
     prefix: classpath:/static/  #thymeleaf翻译页面路径,默认为templates
     cache: false  #是否使用缓存
     mode: HTML  #严格的HTML语法模式
     suffix: .html
   ```

3. 在controller方法中，绑定共享数据，请求转发到页面，完成动态页面显示

   ```Java
   import org.springframework.ui.Model;
   @Controller
   @RequestMapping("/test")
   public class TeatController {
       @RequestMapping("/first")
       public String first(HttpServletRequest request, Model model) {
           //绑定共享数据
           request.setAttribute("name", "维维");
           model.addAttribute("age", 22);
           return "/index.html";
       }
   }
   
   ```

4. 在转发页面中，显示共享数据

   ```html
   <!DOCTYPE html>
   <html xmlns:th="http://www.thymeleaf.org">
   <head>
       <meta charset="UTF-8">
       <title>Title</title>
   </head>
   <body>
   姓名：<span th:text="${name}"></span>
   年龄：<span th:text="${age}"></span>
   </body>
   </html>
   ```

利用thymrleaf显示共享对象的属性值

1. 在controller方法中，绑定共享对象

   ```Java
   request.setAttribute("em", new Emp(1, "vv", 9000));
   ```

2. 页面显示

   ```html
   <!DOCTYPE html>
   <html xmlns:th="http://www.thymeleaf.org">
   <head>
       <meta charset="UTF-8">
       <title>Title</title>
   </head>
   <body>
   姓名：<span th:text="${name}"></span>
   年龄：<span th:text="${age}"></span>
   <hr>
   编号：<span th:text="${em.id}"></span><br>
   姓名：<span th:text="${em.name}"></span><br>
   工资：<input type="text" th:value="${em.money}">
   </body>
   </html>
   ```

在访问对象属性时，是通过get方法完成的属性访问，如果一个实体类里面没有属性，但有get方法，也可以显示。

### thymeleaf条件判断

当th:if条件满足就显示,不支持th:else

```html
<span th:if="${em.money>=8000}">高薪人士</span>
<span th:if="${em.money<8000}">底薪人士</span>
```

### 在超链接中动态显示数据

```html
<a th:href="@{/del(id=${em.id})}">删除</a>
```

### thymeleaf迭代

1. 在方法中绑定集合共享数据

```Java
List<Emp> list = new ArrayList<>();
list.add(new Emp(2, "bb", 100000));
list.add(new Emp(3, "xx", 20000));
list.add(new Emp(4, "hh", 3000));
request.setAttribute("emlist", list);
```

2.在页面中完成迭代

```html
<table width="80%" border="1" cellspacing="0">
    <thead>
        <tr>
            <th>编号</th>
            <th>姓名</th>
            <th>工资</th>
        </tr>
    </thead>
    <tbody>
    <tr th:each="em:${emlist}" }>
        <td th:text="${em.id}"></td>
        <td th:text="${em.name}"></td>
        <td th:text="${em.money}"></td>
    </tr>
    </tbody>
</table>
```

thymeleaf迭代数字

1. 在controller方法中绑定共享的数据

   ```Java
   request.setAttribute("totalPage", 5);
   ```

2. 在页面显示

   ```html
   a th:each="i:${#numbers.sequence(1,totalPage)}"
      th:text="${i}" th:href="@{/cutAll(pageNo=${i})}" style="margin-left: 10px"></a>
   ```

# Ajax

全称（Asynchronous JavaScript and XML）：异步JavaScript和xml，是指一种创建交互式和快速动态网页应用的网页开发技术，无需重新加载这个网页的情况下，能更新部分网页的技术。

Ajax利用javaScript代替传统的表单提交方式，和服务器进行了少量的数据交换，可以将响应信息利用javaScript进行网页数据的部分更新。

Ajax涉及7项技术，JavaScript、XMLHttpRequest、Dom、css、HTML、XML以及相关服务器API。

axios是实现Ajax的js框架
在html中导入axios
<script src="/js/axios.min.js"></script>

发送请求的两种方式
利用axios发出ajax的get请求
```axios.get("/user/checkName",{
    /*请求参数*/
    params:{
        name:$("userName").value
    }
}).then(response =>{/*then用于处理响应信息*/
    /*获取相应信息得消息体*/
    let info=response.data;
    if (info=="ok"){
        $("nameSpan").innerHTML="该用户已存在";
        $("nameSpan").style.color="red";
    }
    else {
        $("nameSpan").innerHTML="可以使用";
        $("nameSpan").style.color="green";
    }
});利用axios发出ajax的post请求
/*创建请求参数对象*/
let paramObj=new URLSearchParams();
/*添加表单数据*/
paramObj.append("name",$("userName").value);
/*向服务器端发送axios的post请求，第一个参数是请求url路径
```

* ```html
第二个参数是表单数据*/
  axios.post("/user/checkName",paramObj).then(
    response =>{/*then用于处理响应信息*/
            /*获取相应信息得消息体*/
            let info=response.data;
            if (info=="ok"){
                $("nameSpan").innerHTML="该用户已存在";
                $("nameSpan").style.color="red";
            }
            else {
                $("nameSpan").innerHTML="可以使用";
                $("nameSpan").style.color="green";
            }
      })
  ```
```

* ```thml
Ajax称为异步的JavaScript和XML。这意味着，当使用Ajax发送请求时，不用等到服务器响应，就可以执行别的任务。而是同步请求时指，需要等到服务器响应信息到达后，才会执行别的任务。
  axios发送的默认是异步请求，但是在开发中，有时候也需要等待服务器信息到达后，再执行某些操作，这是就需要发送同步请求。
  function mytest(){
    let x="1";
    axios.get("/contry/mytest").then(resp=>{
        x=resp.data;
    });
    alert(x);
  }
```

## ajax和传统的表单请求服务器响应区别

传统表单提交请求服务器，会将响应信息刷新整个页面，网页原来的内容全部被清空，ajax提交，利用js对象向服务器发送请求，响应信息也由该对象进行接收，接收完成以后，客户端才可以通过一系列js处理，然后再去构架DOM数，显示响应信息，就是这样才可以实现网页局部更新。

## 文件上传

文件上传是将客户端本地的文字、图片、视频等上传给web服务器。

文件上传，要求客户端必须以post方式提交，并在表单中添加enctype="multipart/form-data"属性。

表单默认情况下"，向服务器提交的是键值对文本数据。

```html
enctype="application/x-www-form-urlencoded 该数据格式只能将文件的文件名发送到服务器，服务器并不能得到客户端上传的文件的二进制数据。
enctype="multipart/form-data" 文件表单数据会将选择的文件的二进制数据上传给服务器。
```

1. 客户表单

   ```html
   <form method="post" action="/user/add" enctype="multipart/form-data">
      姓名：<input type="text" name="name"><br>
      头像：<input type="file" name="file" id="imgFile" onchange="fileChange()"><br>
      图片预览：<img id="imgF" width="200px">
      <input type="submit" value="提交">
   </form>
   ```

2. 编写js

   ```js
   function fileChange() {
       var reader = new FileReader();//定义文件读取流对象
       reader.readAsDataURL(document.getElementById("imgFile").files[0]);
       reader.onload = function (ev) {
           document.getElementById("imgF").src = ev.target.result;
       }
   }
   ```

3. 服务端：

   1. Application.yml添加

      ```yml
      spring:
        servlet:
          multipart:
            max-file-size: 20MB //单个上传文件不能超过
            max-request-size: 100MB //上传文件总大小
      ```

   2. 在controller中完成文件上传

      ```java
      @RestController
      @RequestMapping("/user")
      public class AddController {
            @RequestMapping("add")
            public String add(String name, @RequestParam("file") MultipartFile mf) {
                  if (!mf.isEmpty()) {
                        //得到上传文件的名称
                        String fileName = mf.getOriginalFilename();
                        //将上传的二进制数据保存到服务器本地
                        //重命名文件
                        fileName = System.currentTimeMillis() + fileName.substring(fileName.lastIndexOf("."));
                        try {
                              mf.transferTo(new File("H:\\Springxx\\webStu\\note\\img\\" + fileName));
                        } catch (IOException e) {
                              throw new RuntimeException(e);
                        }
                        return "上传成功";
                  }
                  return null;
            }
      }
      ```

   3. 将服务器本地文件，映射为网络访问的目录文件

      ```java
      @Configuration
      public class MVCConfig implements WebMvcConfigurer {
            @Override
            public void addResourceHandlers(ResourceHandlerRegistry registry) {
                  //http://localhost:8088/face/1676267646140美女.png
                  registry.addResourceHandler("/face/**")
                          .addResourceLocations("file:H:\\Springxx\\webStu\\note\\img\\");
            }
      }
      ```

## 利用ajax上传文件

```html
<!--ajax上传-->
姓名：<input type="text" name="name" id="name"><br>
头像：<input type="file" name="file" id="imgFile2" onchange="fileChange('imgFile2','imgF2')"><br>
图片预览：<img id="imgF2" width="200px">
<input type="button" value="提交" onclick="add()">
```

```js
function add() {
    let formObj = new FormData();
    //文本数据
    formObj.append("name", $("name").value);
    //表单二进制数据
    formObj.append("file", $("imgFile2").files[0]);
    //设置内容请求头
    let config = {
        headers: {'Content-Type': 'multipart/form-data'}
    }
    axios.post("/user/add", formObj, config).then(resp => {
        alert(resp.data)
    });
}
```

# 状态跟踪

http协议使用的是无状态的连接，对容器而言，每一个请求都来自于一个新用户，但是开发中，很多时候需要服务器记录用户的一些信息，从而进行一系列商务活动，这时，就需要就需要跟踪用户的状态，如何让服务器知道两次请求的用户是同一个用户呢？

状态跟踪的解决方案：

1. 隐藏表单
2. cookie
3. session
4. url重写
5. JWT

## 隐藏表单

在表单提交时，会将隐藏表单的内容一起发送给服务器，这时，可以利用隐藏表单发送代表用户的相关信息，服务器在接收用户信息后，如果两次信息一致，，就认为是同一个用户。

隐藏表单的缺陷：

1. 代表每个用户唯一标识的编码，需要开发者进行处理。
2. 当用户在访问服务器时，都必须通过隐藏表单，附加代表用户的唯一标识的编码，开发变得很繁琐。

## cookie

cookie是服务器发送给客户端的一段文本，用于记录用户的相关的信息。

cookie的工作流程：

客户端访问服务器，服务器调用`response.addCookie(new Cookie("name", "vv"));`,设置cookie信息。在参生响应时，会参生set-Cookie响应头，在该响应头附加Cookie键值对，客户端浏览器在得到服务器响应信息后，会将cookie信息在浏览器中缓存起来，当客户端再次请求，会将之前发生的cookie信息再一次以请求头的方式发送给服务器，服务器根据cookie信息跟踪浏览器。

浏览器在接收cookie信息后，无论访问服务器什么资源都会将cookie信息发送给服务器，这样，就不用在每个请求信息中附加用户信息。

### 设置和得到cookie信息

```java
//设置cookie信息
response.addCookie(new Cookie("name", "vv"));


//得到客户端所有的cookie信息
Cookie[] cookies = request.getCookies();
```

### Cookie的分类

cookie的分为两种：

1. 将cookie保存在浏览器的缓存中，在浏览器不关闭，可以一直将cookie信息发送给服务器，浏览器一关闭cookie信息消失。（默认）

2. 如果在发送cookie信息时，设置了过期时间，那么，将cookie信息保存在客户端的文件中，过期时间内，即使用户关闭了浏览器，也可以将cookie信息发送给服务器，超过过期时间，cookie消失。

   ```java
   Cookie cookie = new Cookie("age", "23");
   //设置cookie的过期时间
   cookie.setMaxAge(200);
   response.addCookie(cookie);
   ```

cookie信息会文本方式保存在客户端，会引起安全隐患，所以，在cookie信息中，不应该被保存敏感和隐秘信息。

cookie的缺陷：使用cookie进行状态跟踪，不用在每次请求时由开发者附加用户信息，但是，区分每个用户的唯一标识算法，同样得到开发者进行。

## session

Session是服务端用于保存用户信息的对象。

session的工作流程：

客户端访问服务器，服务器调用request.getSession();创建一个跟踪用户状态的session对象。为了区分不同用户，容器为每个session对象都分配了一个唯一标识session。然后以sessionid为键，以session对象为值，保存进服务器存放session的map集合，在产生响应时，会将sessionid以响应头的方式发给客户端。

当客户端再次请求，会将sessionid以cookie请求头的方式发送给服务器，服务器在接收到sessionid以后会从map集合中，得到对应的session对象，从而跟踪状态。

### req.getSession()含义

```java
//得到session对象
HttpSession session = request.getSession();

return "speak：" + session.getId();
```

当用户调用req.getSession(),时，web容器会从保存session的map集合中查找出来，是否有该用户对应的session对象，如果有则返回，没有就新建session对象然后保存map集合中，再返回。

这样，同一个用户无论访问服务器多少次，都会得到同一个session对象。

在HTTPSession中，提供了setAttribute（）和getAttribute（）方法中，绑定共享数据在会话范围内共享。

```java 
//绑定
session.setAttribute("name", "vv");
//取出
 String name = (String) req.getSession().getAttribute("name");
//移除共享数据
request.getSession().removeAttribute("loginUser");
```

### session销毁

http协议是无状态的协议，当用户关闭浏览器下线时，服务器并不清楚。因而还会保留分配给该用户的session对象，所以关闭浏览器，硬不意味着对session的销毁。

如果不及时关闭session对象，服务器的内存会被持续占用，持续消耗，新用户session又在不断创建，最后服务器崩溃。

销毁方式：

1. 设置超时时间
2. 调用invalidate（）强行销毁session
3. 应用程序结束或服务器崩溃

### 调用invalidate（）强行销毁session

```
request.getSession().invalidate();
```

#### 设置超时时间

```
server:
  port: 8088
  servlet:
    session:
      timeout: PT10M
```

D是天

T天和小时之间

H小时

M分钟

S秒

每个单位必须是数字，且不能颠倒。

### session常见方法

返回容器最后一次得到该会话的请求id的时间

getLastAccessedTime（）

对于会话指定客户请求最大时间间隔，以秒为单位，-1表示用不过期

setMaxInactiveLnterval(int interval)

会话结束，当前存在在会中的所有会话属性也会解除绑定。

invalidata

返回每个session唯一标识

getId（）

## url重写

由于使用cookie会存在一些安全隐患，有些用户会在客户端浏览时对cookie进行禁用，这样，当使用session进行会话跟踪的时候，由于不能使用cookie，那么也就无法通过cookie方式向服务器发送sessionId.

服务器得不到sessionId，会认为这是一个新用户，那么会重写一个新session对象，这样无法进行会话跟踪。

url重写可以在请求的url地址中，添加sessionId，这样即使客户对cookie进行禁用，服务器同样可以得到sessionId，从而跟踪状态。

```java
<a href="/look;jsessionid=2343453">vv</a>

//重写，将sessionId放在url后面

String url=response.endcodeRedirectURL("/look/sucess.html")
```

## cookie和session的区别

1. cookie只能存储文本信息，而session可以绑定对象信息
2. cookie信息存储在客户端，而session信息存储在服务器，所有cookie会有一些安全隐患，而session信息要安全很多。
3. cookie使用时在客户端和服务器之间交互的是具体的数据，而session使用时，在客户端和服务器之间只交互sessionId
4. session是服务器对象，所有创建session对象会占用服务器内存，而cookie不会占用内存。

easypoi可以方便的写出Excel导出，excel数据导入，允许用户上传excel中的数据。

## excel文件上传

1. 添加依赖

   ```
   <dependencies>
       <dependency>
           <groupId>cn.afterturn</groupId>
           <artifactId>easypoi-base</artifactId>
           <version>3.2.0</version>
       </dependency>
       <dependency>
           <groupId>cn.afterturn</groupId>
           <artifactId>easypoi-web</artifactId>
           <version>3.2.0</version>
       </dependency>
       <dependency>
           <groupId>cn.afterturn</groupId>
           <artifactId>easypoi-annotation</artifactId>
           <version>3.2.0</version>
       </dependency>
   </dependencies>
   ```

2. 编写实体类

   ```
   @ExcelTarget("courseEntity")
   public class UserBean {
         @Excel(name = "工号")
         private String id;
         @Excel(name = "姓名")
         private String name;
         @Excel(name = "职位")
         private String job;
         @Excel(name = "工资")
         private Integer money;
         @Excel(name = "生日")
         private String birthday;
         }
   ```

3. 编写controller

   ```
   @RestController
   @RequestMapping("/excel")
   public class UserController {
         @RequestMapping("add")
         public List<UserBean> add(MultipartFile file) throws Exception {
               int t = 1 / 0;
               if (!file.isEmpty()) {
                     //设置初始化信息
                     ImportParams params = new ImportParams();
                     //设置选选项卡
                     params.setSheetNum(1);
                     //标题栏占一行
                     params.setTitleRows(1);
                     //设置表头占用的行数
                     params.setHeadRows(1);
                     //读取上传文件的信息，封装成集合
                     //上传文件的二进制流
                     //实体类类模板
                     //初始化信息
                     List<UserBean> list = ExcelImportUtil.importExcel(file.getInputStream(), UserBean.class, params);
                     return list;
               }
               
               return null;
         }
   }
   ```

4. 编写页面

   ```
   <form action="/excel/add" method="post" enctype="multipart/form-data">
      请选择员工的excel：<input type="file" name="file"><br>
      <input type="submit" value="上传">
   </form>
   ```

5. 异常工具

   ```
   @ControllerAdvice
   public class PressException {
         @ExceptionHandler(Exception.class)
         public String press(Exception e) {
               System.out.println("有异常" + e.getClass().getSimpleName());
               return "redirect:/err.html";
         }
   }
   ```

6. 编写excel

7. 编写启动类

## @ControllerAdvice和@RestController区别

@ControllerAdvice对于处理返回的字符串，会请求转发啊或重定向到err页面

@RestController返回字符串原样输出

# 拦截器

spring MVC的拦截器(Interceptor)与Java servlet的过滤器(Filter)类似，他主要用于；拦截用户请求并做相应的处理，通常应用于权限验证、记录请求信息的日志、判断用户是否登录等功能。
新建拦截器类
public class LoginInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        UserBean user=(UserBean) request.getSession().getAttribute("loginUser");
        if (user==null){
            response.getWriter().print("noland");
            return false;
        }
        return true;
    }
}HandlerInterceptor提供的三个方法：
preHandle:该方法执行controller方法执行之前执行，返回Boolean值,如果返回true，标识可以继续访问目标资源。如果返回false，则停止访问目标资源。
postHandle:该方法在preHandle返回true的时候执行，在controller方法执行后，渲染页面之前执行。
afterCompletion:该方法在preHandle返回true的时候执行，该方法在渲染页面完成后执行。
定义配置类 完成拦截器的注册
@Configuration
public class MVCConfig implements WebMvcConfigurer {
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        //定义客户端什么样的访问URL，需要经过指定的拦截器
        registry.addInterceptor(new LoginInterceptor()).addPathPatterns("/speak/*");
    }
}

# 客户端校验

客户端校验是指，通过Javascript脚本语言对提交服务器表单数据进行校验。校验通过，再将表单数据提交给服务器。
客户端校验有点是直观，可以再不访问服务器的情况下们直接对非法数据的错误直接再页面显示出来。
客户端校验的缺点是并不安全，因为客户端校验需要依赖于浏览器和JavaScript。很多时候，可以绕过浏览器和JavaScript，直接访问服务器。
服务器校验
服务器校验是指，再服务器接收到客户端接收到客户端提交的表单数据后，在访问业务组件前，进行的数据验证。验证通过，则访问业务组件，否则，给客户端发送错误提示。
服务器端校验优点是比较安全，是客户端数据访问业务组件的最后一道屏障。
服务器端校验缺点是执行效率低。多了访问服务器的步骤，校验由服务器完成，所以服务器压力要大一些。

1. 添加依赖

   ```
   <dependency>
               <groupId>org.hibernate.validator</groupId>
               <artifactId>hibernate-validator</artifactId>
               <version>6.1.5.Final</version>
           </dependency>
           <dependency>
               <groupId>org.glassfish</groupId>
               <artifactId>javax.el</artifactId>
               <version>3.0.1-b11</version>
           </dependency>
   
   ```

2. 编写实体类

   ```java
   public class RootBean {
         private Integer id;
         @NotBlank(message = "用户名必须填写")//校验不能为空
         private String name;
         @NotBlank(message = "密码必须填写")
         @Length(min = 6, max = 10, message = "密码长度必须6-10位")//校验长度
         private String pwd;
         @Pattern(regexp = "1[35789]\\d{9}", message = "电话必须以13、15、17、18、19开始的11位数")//校验正则表达式
         private String phone;
         @NotNull(message = "工资必须填写")
         @Max(value = 20000, message = "工资太高")
         @Min(value = 5000, message = "工资太低")
         private Integer money;
   ```

   

3. 编写controller

   ```
   @RequestMapping("add2")
         @ResponseBody
         public String add(@Valid RootBean rootBean, BindingResult result) throws JsonProcessingException {
               if (result.hasErrors()) {
                     //得到错误信息集合
                     List<FieldError> list = result.getFieldErrors();
                     ObjectMapper om = new ObjectMapper();
                     //将集合转换成json字符串
                     String str = om.writeValueAsString(list);
                     return str;
               }
               System.out.println("执行业务操作");
               return "ok";
         }
   ```

4. 编写js

   ```
   <script src="/js/axios.min.js"></script>
   <script src="/js/uitl.js"></script>
   <script>
      function add() {
         $("nameSpan").innerHTML = "";
         $("pwdSpan").innerHTML = "";
         $("moneySpan").innerHTML = "";
         $("phoneSpan").innerHTML = "";
         axios.get("/excel/add2", {
            params: {
               name: $("name").value,
               pwd: $("pwd").value,
               money: $("money").value,
               phone: $("phone").value
            }
         }).then(resp => {
            var info = resp.data;
            console.log(info)
            if (info == "ok") {
               alert("添加成功")
            } else {
               for (let i = 0; i < info.length; i++) {
                  var errs = info[i];
                  $(errs.field + "Span").innerHTML = errs.defaultMessage;
               }
            }
         })
      }
   </script>
   <body>
   <input type="text" id="name" placeholder="用户名"><span id="nameSpan"></span><br>
   <input type="password" id="pwd" placeholder="密码"><span id="pwdSpan"></span><br>
   <input type="text" id="money" placeholder="工资"><span id="moneySpan"></span><br>
   <input type="text" id="phone" placeholder="电话"><span id="phoneSpan"></span><br>
   <input type="button" value="提交" onclick="add()">
   </body>
   </html>
   ```

# VUE

是一种套件用户界面的前端渐进式框架。

vue是model-view-viewModel的缩写。

Model:代表数据类型，也可以在model中定义数据修改和操作的业务逻辑。

view：代表UI组件，它负责将数据模型转化成UI展现出来。

viewModel：监听视图模型改变和操控视图行为，处理用户交互，简单理解就是一个同步view和model的对象，连接model和view。

在MVVM框架下，View和model之间并没有直接的关系，而是通过ViewModel进行交互，Model和ViewModel之间的交互是双向的，因此view数据的变化会同步到model中，而model数据的变化会立即反应到view上。

## 环境搭建

1. 下载安装node.js

   nodel.js是一个让JavaScript运行于服务端的开发平台，他可用于方便地搭建，响应速度快。

   设置镜像：前端开发库，需要设置拉取镜像地址，国内的服务器速度较快，通常设置为淘宝镜像

   npm config set registry=https://registry.npm.taobao.org

   npm config list

2. idea中安装vue的插件，vuejs

   创建vue项目

   下载，终端进入模块，下载npm install --save axios  vue-router echarts  element-plus @element-plus/icons-vue

   运行上面下载的：npm run serve

   关闭：ctrl+c  然后y

3. 目录结构

   node_modules:插件目录

   public：静态资源

   src:源码目录

   ​	assets:存放图片、js、css等静态资源

   ​	components:存放可重用的全局组件

   ​	main.js入口文件

   babel.config.js:转换插件

   package-lock.json、package.json 为npm的配置文件

4. 修改配置，以及方法

   vue.config.js

   ```
   const { defineConfig } = require('@vue/cli-service')
   module.exports = defineConfig({
     transpileDependencies: true,//忽略未使用变量或组件报错问题
     devServer:{
       port:8082 //改变端口
     }
   })
   ```

   src下面的main.js为入口文件，在服务器启动的时候自动完成加载，访问服务器时执行该文件

   vue的组成：

   ```vue
   //模型视图单向绑定
   	<template>存放html
   	<script>存放js
   	<style>存放css代码
   	<template>
     	<div style="width:1000px;margin:auto">
       	用户名：<input type="text"><br>
       	密码：<input type="password"><br>
       	<input type="button" value="登录">
       	<p>
       	//在页面显示script里的值：用{{name}}获取
         	用户名：{{name}}----密码：{{pwd}}
       	</p>
     	</div>
   	</template>
   	在script标签中添加模型
   	<script>
   	export default {
     	data() {
       	return {
         	name:"维维",
         	pwd:"111"
       	}
    	 }
   	}
   	</script>
   
   	<style scoped>
   
   	</style>
   
   
   在main.js中，导入Login.vue组件，将Login.vue作为访问的首页
   	import { createApp } from 'vue'
   	import loginStu from './loginStu.vue'//表示当前目录下查找
   
   	createApp(loginStu).mount('#app')
   
   
   
   //模型和视图的双向绑定
   	双向绑定的含义：当模型内存发生变化时，视图内容也随之变化，同样，当视图内容发生变化，模型内容也发生变化。
   //普通文本
   	<input type="text" v-model="name"><br>
   //html标签
   	<div v-html="content"></div>
   	data里面：
   	name:"维维",
   	content:"<span style='color: pink'>hh</span>"
   
   //绑定属性
   	<div :style="divCss">div内容</div>
   	或
   	<div v-bind:style="divCss">div内容</div>
   	data里面：
   	divCss:"background-color:yello
   
   //图片
    	<img width="200" height="150" :src="img">
   	data里面:
   	img:require("@/img/美女.png") //@表示回到src根目录
   
   //单选框：
    	性别：<input type="radio" v-model="sex" value="男">男
       	<input type="radio" v-model="sex" value="女">女<br>
   	deda:
    	sex:"男" //默认男
   
   //多选框：
    	爱好：<input type="checkbox" v-model="like" value="sport">1
       <input type="checkbox" v-model="like" value="sport1">2
       <input type="checkbox" v-model="like" value="sport2">3
       <input type="checkbox" v-model="like" value="sport3">4<br>
   	data里面：
    	like:[]
   
   //下拉框：
    	学历：
       <select v-model="stu">
         <option value="1">1</option>
         <option value="2">2</option>
         <option value="3">3</option>
       </select>
   	data里面：
    	stu:""
   
   //vue事件：
    	用户名：<input type="text" v-model="name"><br>
    	密码：<input type="password"  v-model="pwd"><br>
       	 <input type="button" value="登录" @click="land()">
   	在data后面写：//land为事件函数名
     	data{},
    	 methods:{
       	/*land:function (){
       	}或者*/
       	land(){
         	if(this.name=="vv" &&this.pwd=="111"){
           	alert("登录成功");
         	}else {
           	alert("登录失败")
         	}
           }
    	 }
   
   //vue条件判断：条件满足才显示，不满足就不会渲染
   	成绩<input type="text" v-model="grade">
       <span v-if="grade>=80">优良</span>
       <span v-else-if="grade>=60">中等</span>
       <span v-else>不行</span>
   	data里面：
   	grade:""
   //vue循环
   	选择班级：<select v-model="classId">
                   <option v-for="classObj in classList" 			:key="classObj.id">{{classObj.name}}	</option>
               </select><br>
       {{classId}}
   	data里面：
   	classId:"" ,//取下拉选项的id值
       classList:[{id:1,name:"vv"},{id:2,name: "bb"},{id:3,name:"xx"}]//下拉列里面的值
   ```


每个vue实例在被创建时都需要经过一系列的初始化过程——例如，需要设置数据监听、编译 模板、将实例挂载到dom并在数据变化时更新DOM等。同时在这个过程中也会运行一些叫做生命周期钩子的函数，这给了用户在不同阶段添加自己的代码的机会。

vue生命周期分为四大类：
创建——created
挂载，将数据渲染到页面上——mounted
更新，在数据更改时调用——updated
销毁——unmounted
每个阶段都有一个beforeXX方法，在执行 某个阶段之前执行。

## vue的路由

在web开发中，路由是指根据url分配到对应的处理程序，对于大多数单页面应用，都推荐使用官方支持的vue-router。vue-router通过管理URL，实现Url和组件的对应，以及通过url进行组件之间的切换。

路由开发步骤

1. 在src目录，新建router目录，新建index.js 和routes.js文件

2. 在routes.js中，定义路由数组，指定组件和映射关系

   ```js
   const routes = [
       {name:"Login",//路由名称
           path:"/login",//访问路径
           component: () => import('@/loginStu')//组件路径
       },
       {name:"Index",
           path:"/index",
           component: () => import('@/index')}];
   
   export default routes ;
   ```

3. 在index.js中导入vue-router和routes.js

   ```js
   import {createRouter,createWebHistory} from 'vue-router'
   import  routes  from "./router"
   
   const router = createRouter({
       history:createWebHistory(),
       routes
   });
   export default router ;
   ```

4. 在app.vue中定义路由视图

   ```vue
   <template>
     <div>
   <!--    路由视图-->
       <router-view/>
     </div>
   </template>
   
   <script>
   
   </script>
   
   <style>
   
   </style>
   ```

5. 在main.js中，导入并使用路由视图和路由js文件

   ```js
   import { createApp } from 'vue'
   import App from './App.vue'
   import router from "./router/index";
   
   createApp(App).use(router).mount('#app')
   ```

   

## 使用标签跳转页面

```vue
//两种皆可
<router-link to="/index">首页index</router-link><br>
<a href="/login">跳转login</a>
//有参数
<router-link :to="{name:'Index',query:{id:2}}">首页index</router-link><br>
```

```vue
this.$router.push("/index");
/*或者*/
/*location.herf="/index";*/
```

使用js代码跳转组件

```vue
//不传参
this.$router.push("/index");
/*或者*/
location.herf="/index";
//传参
this.$router.push({name:'Index',query:{id:2}});
location.herf="/index?id=3";
```

接收传的参数

```
 id:{{id}}

 id:""
 
 created() {
    this.id=this.$route.query.id;
  }
```

## 嵌套路由

在一个路由里面嵌套另一个路由，类似于iframe框架网页

1、创键两个子vue

2、修改router.js

```
{name:"Index",
    path:"/index",
    component: () => import('@/index'),
children:[
    {name:"user",
        path:"/index/user",
        component: () => import('@/user/UserInd.vue')
    },
    {name:"root",
        path:"/index/root",
        component: () => import('@/room/RoomInd.vue')
    }
]}
```

3、在index.vue里面修改

```
<div style="width: 1000px; margin: auto;text-align: center ;display: flex" >
  <div style="width: 200px;">导航栏</div>
  <div style="width: 800px;"><router-view/></div>

</div>
```

## 跨域问题

跨域问题来自浏览器安全机制，浏览器安全的基石是同源政策，同源政策的目的，是为了保证用户信息的安全，防止恶意的网站窥觑数据。

同源政策是防止cookie在不同服务器之间共享，以确保cookie的安全性。

如果非同源有三种限制：cookie信息无法获取，dom不能获取，ajax请求无法发送。

在使用vue-cli时，采用了nodejs作为前端服务器，这时，如果需要在前端服务器中访问后台服务器，就会有跨域问题。

在axios中，可以通过配置代理，解决跨域问题。

客户端请求服务器是存在跨域问题的，而服务器与服务器之间可以互相请求数据，是没有跨域的概念。

所以，所以，可以配置一个代理的服务器可以请求另一个服务器的数据，然后请求的数据返回代理服务器中，代理服务器在返回给客户端，就实现了跨域访问的问题。

## 实例

1. 首先配置后端路径vue.config.js

   ```
   devServer:{
     port:8080 ,//改变端口
     proxy: {
       "/project": {
         target: "http://localhost:8088",
         pathRewrite: {
           "^/project": "/"
         }
       }
     }
     }
   ```

2. 添加路由

   ```
   {name:"student",
       path:"/index/student",
       component: () => import('@/student/index.vue')
   }
   ```

3. 编写页面和功能

   ```vue
   <template>
     <div>
       <table width="80%" cellspacing="0" border="1">
         <thead>
         <tr>
           <th>姓名</th>
           <th>性别</th>
           <th>生日</th>
           <th>学历</th>
           <th>爱好</th>
           <th>操作</th>
         </tr>
         </thead>
         <tbody>
         <tr v-for="student in cutObj.records" :key="student.id">
           <td>{{student.name}}</td>
           <td>{{student.sex}}</td>
           <td>{{student.birthday}}</td>
           <td>{{student.end}}</td>
           <td>{{student.likes}}</td>
           <td><input type="button" value="修改" @click="findById(student.id)">
             <input type="button" value="删除" @click="del(student.id)"></td>
         </tr>
         </tbody>
       </table>
       <input type="button" v-for="n in cutObj.pages" :value="n" style="margin-left: 10px" @click="findByItem(n)" :key="n"><br>
       姓名<input type="text" v-model="findObj.name"><br>
       开始生日<input type="text" v-model="findObj.startDate"><br>
       结束生日<input type="text" v-model="findObj.endDate"><br>
       <input type="button" value="查找" @click="findByItem(1)"><br>
       <input type="button" value="添加" @click="showAdd=true"><br>
       <div class="dialog" v-if="showAdd">
         <div class="content">
           <input type="text" v-model="addObj.name" placeholder="姓名"><br>
           性别：<input type="radio" name="sex" v-model="addObj.sex" value="男">男
           <input type="radio" name="sex" v-model="addObj.sex" value="女" >女<br>
           学历：
           <select v-model="addObj.end">
               <option>初中</option>
               <option>高中</option>
               <option>大学</option>
           </select><br>
           <input type="text" v-model="addObj.birthday" placeholder="生日"><br>
           爱好：
           <input type="checkbox" v-model="addObj.likeArray" value="体育">体育
           <input type="checkbox" v-model="addObj.likeArray" value="音乐">音乐
           <input type="checkbox" v-model="addObj.likeArray" value="美术">美术<br>
             
             //图片预览效果
           <img width="200" height="200" :src="addObj.imagePath">
           <input type="file" id="myFile" @change="imgChange"><br>
             
             
           <input type="button" value="添加" @click="add"><br>
         </div>
       </div>
       <div class="dialog" v-if="showUpdate">
         <div class="content">
             //展示服务器图片路径
           <img :src="'/project/face/'+findByIdObj.head" width="20"><br>
           姓名：{{findByIdObj.name}}<br>
           性别：{{findByIdObj.sex}}<br>
           学历：
           <select v-model="findByIdObj.end">
             <option>初中</option>
             <option>高中</option>
             <option>大学</option>
           </select><br>
           生日：{{findByIdObj.birthday}}<br>
           爱好：{{findByIdObj.likes}}<br>
           <input type="button" value="修改" @click="update()">
          <br>
         </div>
       </div>
     </div>
   </template>
   
   <script>
   import axios from "axios"
   export default {
     data() {
       return {
         showUpdate:false,
         showAdd: false,
         addObj: {likeArray: []},
         cutObj: {},//分页对象
         findObj: {} ,//分页查询条件
         findByIdObj:{end:""}
       }
     },
     methods: {
       findByItem(pageNO) {
         this.findObj.pageNO = pageNO;
         axios.get("/project/student/findByItem", {params: this.findObj})
             .then(resp => {
               this.cutObj = resp.data;
               this.findObj.name="";
               this.findObj.startDate="";
               this.findObj.endDate=""
             })
       },
       imgChange() {
           //ajax请求上传文件
         var reader = new FileReader();
         reader.readAsDataURL(document.getElementById("myFile").files[0]);
         reader.onload = (ev) => {
           this.addObj.imagePath = ev.target.result;
         }
       },
       add() {
         var formObj = new FormData();
         formObj.append("name", this.addObj.name);
         formObj.append("sex", this.addObj.sex);
         formObj.append("birthday", this.addObj.birthday);
         formObj.append("end", this.addObj.end);
         formObj.append("likeArray", this.addObj.likeArray);
         formObj.append("face", document.getElementById("myFile").files[0]);
         let config = {
           headers: {'Content-Type': 'multipart/form-data'}
         };
         axios.post("/project/student/add", formObj, config).then(resp => {
           if (resp.data == "ok") {
             this.showAdd = false;
             this.findByItem(1);
             this.addObj.name="";
             this.addObj.sex="";
             this.addObj.birthday="";
             this.addObj.end="";
             this.addObj.likeArray="";
           }
         })
       },
       findById(id) {
         axios.get("/project/student/findById",{params:{id}})
             .then(resp=>{
               this.findByIdObj=resp.data;
               this.showUpdate=true;
             })
   
       },
       update(){
         axios.get("/project/student/update",{params:{
           id:this.findByIdObj.id,
             edu:this.findByIdObj.end
           }}).then(resp=>{
             if(resp.data=="ok"){
               this.showUpdate=false;
               this.findByItem(1);
             }
         })
       },
       del(id){
         axios.get("/project/student/del",{params: {id}})
             .then(resp=>{
               if(resp.data=="ok"){
                 this.findByItem(1);
               }
             })
       }
     },
     created() {
       this.findByItem(1);
     }
   }
   </script>
   
   <style scoped>
   .dialog{
     width: 100%;
     height: 100%;
     background-color: rgba(246, 160, 160, 0.8);
     display: flex;
     align-items: center;
     justify-content: center;
     position: fixed;
     left: 0;
     top: 0;
   
   }
   </style>
   ```

4. 如果发送复选框数据，或者多个同名键值对，通常会以数组方式发给服务器，这是必须用post提交，才能发送多个同名键值对。

   ```vue
   var paramObj=new URLSearchParams();
   paramObj.append("idArray",this.paramObj.IdArray);
   axios.post("路径"，paramObj).then(resp=>{...});
   ```

   

## 导入elementUI和elementUI的图标库

在main.js中，导入。

```js
import ElementUI from 'element-plus';
import 'element-plus/theme-chalk/index.css';


import * as ElIcons from '@element-plus/icons-vue'

const app = createApp(App);
app.use(router).use(ElementUI).mount("#app");

for(const iconName in  ElIcons){
    app.component(iconName,ElIcons[iconName]);
}
```

### 布局

elementUI布局。横拍为24

```vue
<el-row>
  <el-col :span="8">网格1</el-col>
  <el-col :span="16">
    <el-row>
      <el-col :span="6">网格21</el-col>
      <el-col :span="6">网格22</el-col>
      <el-col :span="6">网格23</el-col>
      <el-col :span="6">网格24</el-col>
    </el-row>
  </el-col>
</el-row>
```

### 按钮

```
<el-row>
  <el-button>默认按钮</el-button>
  <el-button type="primary">主要按钮</el-button>
  <el-button type="success">成功按钮</el-button>
  <el-button type="info">信息按钮</el-button>
  <el-button type="warning">警告按钮</el-button>
  <el-button type="danger">危险按钮</el-button>
</el-row>

<el-row>
  <el-button plain>朴素按钮</el-button>
  <el-button type="primary" plain>主要按钮</el-button>
  <el-button type="success" plain>成功按钮</el-button>
  <el-button type="info" plain>信息按钮</el-button>
  <el-button type="warning" plain>警告按钮</el-button>
  <el-button type="danger" plain>危险按钮</el-button>
</el-row>

<el-row>
  <el-button round>圆角按钮</el-button>
  <el-button type="primary" round>主要按钮</el-button>
  <el-button type="success" round>成功按钮</el-button>
  <el-button type="info" round>信息按钮</el-button>
  <el-button type="warning" round>警告按钮</el-button>
  <el-button type="danger" round>危险按钮</el-button>
</el-row>

<el-row>
  <el-button  circle ><el-icon><Setting /></el-icon></el-button>
  <el-button type="primary"  circle><el-icon><Setting /></el-icon></el-button>
  <el-button type="success" icon="el-icon-check" circle></el-button>
  <el-button type="info" icon="el-icon-message" circle></el-button>
  <el-button type="warning" icon="el-icon-star-off" circle></el-button>
  <el-button type="danger" icon="el-icon-delete" circle></el-button>
</el-row>
```

### 输入框

```
<el-row style="margin-top: 20px;display: flex;align-items: center;margin-bottom: 20px;">
  <el-col :span="4"> 姓名：</el-col>
  <el-col :span="20"><el-input v-model="input" placeholder="请输入内容" style="width: 100px;"></el-input></el-col>
</el-row>
<el-input
        placeholder="请输入内容"
        v-model="input"
        :disabled="true" style="width: 100px">
    </el-input>
```

### 单选框

```
<el-radio-group v-model="sex">
  <el-radio label="线上品牌商赞助"></el-radio>
  <el-radio label="线下场地免费"></el-radio>
</el-radio-group>
```

### 复选框

```
<el-checkbox-group v-model="likearray">
  <el-checkbox label="美食/餐厅线上活动" name="type"></el-checkbox>
  <el-checkbox label="地推活动" name="type"></el-checkbox>
  <el-checkbox label="线下主题活动" name="type"></el-checkbox>
  <el-checkbox label="单纯品牌曝光" name="type"></el-checkbox>
</el-checkbox-group>
{{likearray}}
```

label="地推活动"为value的值，绑定的是likearray

### 下拉框

```
<el-form-item label="活动区域">
  <el-select v-model="region" placeholder="请选择活动区域">
    <el-option :label=cl.name :value=cl.id :key=cl.id  v-for="cl in classArray"></el-option>
  </el-select>
</el-form-item>
{{region}}



 region:"",
      classArray:[
        {id:1,name:"vv"},
        {id:2,name:"bb"},
        {id:3,name:"xx"},

      ]
```

### 日期选择器：

```
导入汉化
import zhCn from 'element-plus/dist/locale/zh-cn.mjs'
app.use(router).use(ElementUI,{ locale: zhCn,}).mount("#app");
//加入日期组件
<el-date-picker
    v-model="value1"
    type="date"
    placeholder="选择日期"
value-format="YYYY-MM-DD">
</el-date-picker>
{{value1}}

//
value1:""
```

### 表格

data:

```
classArray:[
  {id:1,name:"vv"},
  {id:2,name:"bb"},
  {id:3,name:"xx"},

],
value1: '',
tableData: [{
  date: '2016-05-02',
  name: '王小虎',
  province: '上海',
  city: '普陀区',
  address: '上海市普陀区金沙江路 1518 弄',
  zip: 200333
}
```

div

```
<el-table
    :data="tableData"
    border
    style="width:80%">
  <el-table-column
      fixed
      prop="date"
      label="日期">
  </el-table-column>
  <el-table-column
      prop="name"
      label="姓名"
      >
  </el-table-column>
  <el-table-column
      fixed="right"
      label="操作"
      >
    <template #default="scope">
      <el-button @click="handleClick(scope.row)" type="text" size="small" link>查看</el-button>
      <el-button type="primary" size="small" link>编辑</el-button>
      <el-button type="danger" size="small" link @click="del(scope.row)">删除</el-button>
    </template>
  </el-table-column>
</el-table>
```

scope.row表示当前对象

方法：

```
del(row){
  console.log(row);
}
```

### 分页

```
<el-pagination
    v-model:current-page="pageNO"
    v-model:page-sizes="pages"
    @current-change="findByItem"
    background
    :total=100
   >
</el-pagination>



 pages:30,
 pageNO:1,
      
      
findByItem(pageNO){
      alert(pageNO)
   }
```

## vue 绑定数据

```
sessionStorage.getItem("loginUser")
sessionStorage.setItem("loginUser","")
sessionStorage.remove("loginUser")
```

## 导航守卫

为了防止非法用户对资源的访问，web服务器通过过滤器和拦截器，进行客户端请求的拦截，身份验证后继续访问。

不过，这种方式只能防止非法用户对服务器资源的访问。

前置守卫

```
//to 目标路由组件
//from  路由组件
//next 表示是否继续访问目标资源
router.beforeEach((to,  from , next) => {
//to.path得到目标资源路由的访问路径，to.name得到目标路由的名称
        var info = sessionStorage.getItem("loginUser");
        if((to.path=="/shop/shop" || to.path == "/shop/manageIndex" || to.path == "/shop/send") && info == null){
            router.push("/shop/login");
        }
        else{
            next();
        }
    }
```

# ECharts

一个使用javascript实现的开源可视化库，提供直观，交互丰富，可高度个性化定制数据可视化图表。

官网

https://echarts.apache.org/zh/index.html

## 柱状图

```
<div id="main" style="width: 600px;height:400px;"></div>

//引入echarts
import * as echarts from 'echarts';
export default {
  data() {
    return {}
  },
  methods:{
    initCharts(){
      var myChart = echarts.init(document.getElementById('main'));
// 绘制图表
      myChart.setOption({
        title: {
          text: 'ECharts 入门示例'
        },
        tooltip: {},
        xAxis: {
          data: ['衬衫', '羊毛衫', '雪纺衫', '裤子', '高跟鞋', '袜子']
        },
        yAxis: {},
        series: [
          {
            name: '销量',
            type: 'bar',
            data: [5, 20, 36, 10, 10, 20]
          }
        ]
      });
      //多类柱状图
      var option;
      var more = echarts.init(document.getElementById('moreColumn'));
      option = {
        tooltip: {
          trigger: 'axis',
          axisPointer: {
            type: 'shadow'
          }
        },
        legend: {
          data: ['Forest', 'Steppe', 'Desert', 'Wetland']
        },
        toolbox: {
          show: true,
          orient: 'vertical',
          left: 'right',
          top: 'center',
          feature: {
            mark: { show: true },
            dataView: { show: true, readOnly: false },
            magicType: { show: true, type: ['line', 'bar', 'stack'] },
            restore: { show: true },
            saveAsImage: { show: true }
          }
        },
        xAxis: [
          {
            type: 'category',
            axisTick: { show: false },
            data: ['2012', '2013', '2014', '2015', '2016']
          }
        ],
        yAxis: [
          {
            type: 'value'
          }
        ],
        series: [
          {
            name: 'Forest',
            type: 'bar',
            barGap: 0,
            emphasis: {
              focus: 'series'
            },
            data: [320, 332, 301, 334, 390]
          },
          {
            name: 'Steppe',
            type: 'bar',
            emphasis: {
              focus: 'series'
            },
            data: [220, 182, 191, 234, 290]
          },
          {
            name: 'Desert',
            type: 'bar',

            emphasis: {
              focus: 'series'
            },
            data: [150, 232, 201, 154, 190]
          },
          {
            name: 'Wetland',
            type: 'bar',
            label: "2000-01-01",
            emphasis: {
              focus: 'series'
            },
            data: [98, 77, 101, 99, 40]
          }
        ]
      };
      more.setOption(option)
    }
  },mounted() {
    this.initCharts();
  }
}
```

## 饼图

```
<div id="main" style="width: 600px;height:400px;"></div>
import * as echarts from 'echarts';


export default {
  data() {
    return {}
  },methods:{
    initCharts(){
      var chartDom = document.getElementById('main');
      var myChart = echarts.init(chartDom);
      var option;
      option = {
        title: {
          text: '进销存系统',
          subtext: '2013-02-25',
          left: 'center'
        },
        tooltip: {
          trigger: 'item'
        },
        legend: {
          orient: 'vertical',
          left: 'left'
        },
        series: [
          {
            name: '销售量',
            type: 'pie',
            radius: '50%',
            data: [
              { value: 1048, name: 'Search Engine' },
              { value: 735, name: 'Direct' },
              { value: 580, name: 'Email' },
              { value: 484, name: 'Union Ads' },
              { value: 300, name: 'Video Ads' }
            ],
            emphasis: {
              itemStyle: {
                shadowBlur: 10,
                shadowOffsetX: 0,
                shadowColor: 'rgba(0, 0, 0, 0.5)'
              }
            }
          }
        ]
      };
      myChart.setOption(option);
    }
  },mounted() {
    this.initCharts();
  }
}

```

# 事务失效场景

1. 事务方法访问修饰符非public，导致事务失效
2. spring事务只支持未检查异常（unchecked），不支持已检查异常（checked），如果@Transactional注解的方法抛出的异常是检查异常,解决办法@Transactional（rollback for=Exception.class）
3. 数据库表不支持事务，导致事务失效，只有innoDB引擎才支持事务
4. @Transactional注解所在的类没有被spring管理，导致事务失效。需要加上@Service\@Component等注解
5. catch掉异常以后，没有再次抛出异常，导致事务失效
6. 方法自身的this调用问题，导致事务失效
7. 数据源没有配置事务管理器，导致事务失效
8. 传播类不支持事务导致事务失效，@Transactional（propagation=Propagation.NOT_SUPPORTED）
9. 多线程访问导致事务失效,两个线程访问同一个方法时，获取的数据库数据连接不一样，从而是两个不同的事务
10. 事务方法添加了final修饰符

# springBoot自动装配

就是将第三方的组件自动装载到IOC容器里面，不需要开发人员再去编写相关的配置。

在spring Boot应用里面只需要加上@SpringBootApplication注解就可以实现自动配置，SpringBootApplication他是一个复合注解，真正实现自动装配的注解是@EnableAutoConfiguration注解。

自动装配的实现，归纳为以下三个核心的步骤：

1. 启动依赖注解时，组件中如果包含@Configuration的配置类，在这个配置类里面申明为Bean注解，然后将方法返回值或属性注入到ioc容器。同时，在启动类中包含了@ComponentScan注解，可以扫描并加载启动类所在的包，及子包中的spring组件类
2. SpringBoot会在/META-INF/目录下证据spring.factories文件，然后SpringBoot会自动根据约定，自动使用SpringFactoriesLoader来加载配置文件中的内容
3. Spring获取到第三方jar中的配置以后会调用ImportSelector接口来完成加载

# Mybatis-plus锁机制

多线程操作同一条数据会导致部分操作丢失。

解决方案：利用锁机制，防止在多线程情况下操作丢失的现象。锁机制分为悲观锁和乐观锁。

悲观锁：每次别人拿数据都以为会修改，所以每次有人拿东西的时候都会上锁，所以其他人拿数据会受到阻塞直到他拿到锁。

乐观锁：每次别人拿都认为不会修改数据，所以不会上锁，但是更新的时候会判断一下在此期间别人有没有去更新这个数据，可以使用版本号等机制。

乐观锁适合读取比较频繁的场景，这样可以提高吞吐量。如果出现大量的写入操作，数据发送冲突的可能性就会增加，为了保证数据的一致性，应用层需要不断的重新获取数据，这样会增加大量的查询操作，降低系统的吞吐量。

## 悲观锁的实现：

悲观锁可以使用synchronized关键字和lock接口相关类来实现，在一个线程访问某个方法的时候，别的线程只能等待。

数据库中同样拥有悲观锁的思想，如果在MySql选择select for update语句，那就是悲观锁，在提交之前不允许第三方来修改数据，这当然会造成一定的性能损耗，在高并发的情况下是不可取的。

## 乐观锁的实现

乐观锁利用在数据库中，添加一个版本version字段。

在获取及修改数据时都不需要加锁，但是我们在获取完数据并计算完毕，准备跟新数据时，会检查版本号和获取数据时的版本号是否一致，如果一致就直接跟新。同时将版本号加一。
如果不一致，说明计算期间已经有其他线程修改过这个数据了，那就可以重新获取数据，重新计算，然后再次尝试跟新数据。

1. 数据库表增加列，默认值为1；

   ![image-20230314164353652](C:\Users\60952\AppData\Roaming\Typora\typora-user-images\image-20230314164353652.png)

2. 实体类加属性

   ```Java
   @TableName("t_book")
   public class BookBean {
         @TableId(value = "pk_id", type = IdType.AUTO)
         private Integer id;
         @TableField("b_name")
         private String name;
         @TableField("b_auth")
         private String auth;
         @TableField("b_price")
         private Double price;
         @TableField("b_img")
         private String img;
         @Version
         @TableField("b_version")
         private Integer version;
         }
   ```

3. 添加配置类

   ```Java
   @Configuration
   public class MybatisPlusConfig {
         @Bean
         public MybatisPlusInterceptor mybatisPlusInterceptor() {
               MybatisPlusInterceptor mybatisPlusInterceptor = new MybatisPlusInterceptor();
               mybatisPlusInterceptor.addInnerInterceptor(new OptimisticLockerInnerInterceptor());
               return mybatisPlusInterceptor;
         }
   }
   ```

4. 编写测试类

   ```Java
   @Test
   public void test() {
         BookBean book1 = mapper.selectById(1);
         BookBean book2 = mapper.selectById(1);
         book1.setPrice(book1.getPrice() + 10);
         book2.setPrice(book2.getPrice() - 20);
         //执行第一个数据
         //版本变为2
         mapper.updateById(book1);
         //拿到的版本为1，但当前版本已经变为2
         mapper.updateById(book2);
         //两条数据都执行
         int num = 0;
         while (num == 0) {
               book2 = mapper.selectById(1);
               book2.setPrice(book2.getPrice() - 20);
               num = mapper.updateById(book2);
         }
   }
   ```

# 百度地图

引入已有的定位系统，百度、高德等。然后在开发平台下，指定的经度纬度加上标记和信息窗。

百度地图开发平台：https://lbsyun.baidu.com/

使用时需要先申请秘钥ak:

注册百度账号-控制台-我的应用-创建应用-获取秘钥服务-ak申请-选择应用类型-浏览器端-javascripect API-申请秘钥的referer白名单中填写*

开发流程：

public文件夹下的index.html加入<script type="text/javascript" src="https://api.map.baidu.com/api?v=1.0&&type=webgl&ak=jwadspyce8pI4d1ItNOTVjcYS8GXQDUh"> </script>

创建vue

注册路由：

```
{name:"dt",
    path:"/dt",
    component: () => import('@/dt.vue')}
```

编写dvue:

```
<template>
  <div id="container" style="width: 1000px;height: 1000px"></div>
</template>

methods:{
    initDT(){
    //第一种
     /* var map = new BMapGL.Map("container");//创建地图实例
      var point = new BMapGL.Point(104.091528, 30.641092);//创建点坐标
      map.centerAndZoom(point, 15);//初始化地图，设置中心点坐标和地图级别
      map.enableScrollWheelZoom(true);//开启鼠标滚轮
      //c创建标注
      var marker=new BMapGL.Marker(point);
      map.addOverlay(marker);//添加标注
      marker.onclick=function (){
        alert("朗沃旧地址")
      }
      var opts = {
        width: 250,     // 信息窗口宽度
        height: 100,    // 信息窗口高度
        title: "朗沃旧地址地址"  // 信息窗口标题
      }
      var infoWindow = new BMapGL.InfoWindow("一环路", opts);  // 创建信息窗口对象
      map.openInfoWindow(infoWindow,point);        // 打开信息窗口*/



//第二种：建议掌握
      var map = new BMapGL.Map('container');
//创建地址解析器实例
      var myGeo = new BMapGL.Geocoder();
      map.enableScrollWheelZoom(true);
// 将地址解析结果显示在地图上，并调整地图视野
      myGeo.getPoint('四川省广元市苍溪县', function(point){
        if(point){
          map.centerAndZoom(point, 16);
          map.addOverlay(new BMapGL.Marker(point, {title: '家'}))
          var opts = {
            width: 250,     // 信息窗口宽度
            height: 100,    // 信息窗口高度
            title: "家"  // 信息窗口标题
          }
          var infoWindow = new BMapGL.InfoWindow("四川省广元市苍溪县", opts);  // 创建信息窗口对象
          map.openInfoWindow(infoWindow,point);        // 打开信息窗口
        }else{
          alert('您选择的地址没有解析到结果！');
        }
      }, '广元市')
    }
    
    
  },mounted() {
    this.initDT()
  }
  
```

# Swagger

Swagger2的出现就是为了从根本上解决团队之间API文档交互问题，他作为一个完整的框架，可以生成描述调用可视化的RESTFUL风格的web服务：

接口文档在线自动生成，文档随接口变动而实时更新，节省维护成本；

支持接口在线测试，不依赖得三方工具。

开发步骤：

http://localhost:8082/swagger-ui/index.html

1. 导入依赖

   ```
   <dependency>
       <groupId>io.springfox</groupId>
       <artifactId>springfox-boot-starter</artifactId>
       <version>3.0.0</version>
   </dependency>
   ```

2. application.yal添加

   spring:
     mvc:
       pathmatch:
         matching-strategy: ant_path_matcher

3. 添加配置类

   ```
   @Configuration
   @EnableSwagger2
   public class SwaggerConfig {
         
         private ApiInfo getApiInfo() {
               ApiInfo apiInfo = new ApiInfoBuilder()
                                         .contact(new Contact("成都朗沃信息有限公司",//发布者公司名称
                                                 "http://localhost:8082",//发布者官网
                                                 "1789654@qq.com")) //发布者电子邮箱
                                         .title("进销存管理系统")  //标题
                                         .description("进销存描述，……") //描述
                                         .version("1.1")
                                         .build();
               return apiInfo;
         }
         
         @Bean
         public Docket createRestApi() {
               return new Docket(DocumentationType.SWAGGER_2)
                              .apiInfo(getApiInfo()) //api文档的详细信息
                              .select() //初始化并返回一个API选择构造器
                              .paths(PathSelectors.any()) //设置路径筛选
                              .apis(RequestHandlerSelectors.basePackage(
                                      "com.vv.controller"))// 添加路径选择条件
                              .build();
         }
   }
   ```

4. 启动类

   ```
   @SpringBootApplication
   @EnableOpenApi
   public class MainTest217 {
         public static void main(String[] args) {
               SpringApplication.run(MainTest217.class);
         }
   }
   ```

5. 给控制器加文档   @Api(tags = {"用户的控制器"})

6. 方法描述

   ```
   @ApiOperation(value = "查找", notes = "按id查找")//方法描述
   @ApiImplicitParams(value = {//方法参数描述@ApiImplicitParam(name ="id",value = "id",required = true)，@ApiImplicitParam(name ="id",value = "id",required = true)
           @ApiImplicitParam(name = "id", value = "编号", required = true)
   })
   ```

7. 忽略方法生产API文档   @ApiIgnore

8. 给实体类加api描述

   类名上：@ApiModel(value = "用户", description = "存储用户信息")

   属性上：@ApiModelProperty(value = "用户编号", required = true)
   
   

# Apache Shiro

Apache Shiro是一个强大且易用的java安全框架，执行身份验证、授权、密码和会话管理。使用Shiro的易于理解的API，可以实现认证、加密、授权、安全标签、安全注解、会话管理、缓存等操作。

权限管理，一般根据系统设置的安全规则或者安全策略，用户可以访问而且只能访问自己被授权的资源。
权限管理包括用户身份认证和授权两部分，对于需要访问控制的资源用户首先经过身份认证，认证通过后用户具有该资源的访问权限方可访问。

身份认证，就是判断一个用户是否为合法用户的处理过程，最常用简单的身份验证方法就是系统核对，用户输入的用户名和口令是否合法的过程。也就是登录认证的过程。

授权，就是根据安全规则，授予哪个用户能访问那些资源，主要用于身份验证后分配权限方可访问系统的资源，对于某些资源无权限无法访问。

## shiro有三个核心组件

Subject,SecurityManager和Realms。

Subject:即"当前操作用户"。但是，在Shiro中，Subject这一概念并不仅仅指认，也可以是第三方进程、后台账户或其他类似物。

SecurityManager：用于管理所有用户的安全操作，shiro通过SecurityManager来管理内部组件实例，并通过它来提供安全管理的各种服务。

Realm:提供SecuirtyManager安全认证的权限数据，当对用户执行认证和授权验证时，Shiro会从应用配置的Realm中查找用户及其权限信息。

## shiro授权认证

Subject：主体。访问系统用户，主体可以是用户、程序等、进行认证都称为主体。

Principal:身份证信息，是主体进行身份认证的标识，标识必须具有唯一性，一个主体可以有多个身份，但必须有一个身份。

Credential:凭证信息，是只有自己知道的安全信息，入密码，证书等。

shiro基本操作：

1. 导入依赖

   ```
   <dependency>
       <groupId>org.apache.shiro</groupId>
       <artifactId>shiro-core</artifactId>
       <version>1.8.0</version>
   </dependency>
   ```

2. 在resources目录下添加shiro.ini文件，作为数据来源。

   ```
   [users]
   tom=123
   join=456
   ```

3. 创建测试类

   ```
   public class ShiroTest {
         public ShiroTest() {
               //创建安全管理器
               DefaultSecurityManager manager=new DefaultSecurityManager();
               //设置权限认证的数据来源，为类路径下的shiro.ini
               manager.setRealm(new IniRealm("classpath:shiro.ini"));
               //设置安全管理器
               SecurityUtils.setSecurityManager(manager);
               //得到主体
               Subject subject=SecurityUtils.getSubject();
               //产生用户名密码的一个令牌
               UsernamePasswordToken token=new UsernamePasswordToken("tom","12");
               
               subject.login(token);
               System.out.println("登录成功");
         }
         
         public static void main(String[] args) {
               new ShiroTest();
         }
   }
   ```

登录成功，则打印登录成功。

登录失败则抛出异常：

密码错误：IncorrectCredentialsException

用户名错误：UnknownAccountException

账号密码错误：UnknownAccountException

## shiro自定义realm

shiro提供了信息认证和授权的功能性接口，但是shiro是不会帮我们维护数据的，shiro中的用户信息，以及用户所对应的权限，都是需要我们从数据库查询出来然后传给shiro相对应的接口。虽然shiro中提供了觉得不错Realm，但是他只能查询用户表，在开发中，为了满足我们的需求，我们必须自定义realm。

```
public class CustomRealm extends AuthorizingRealm {
      /**
       * 用于授权的方法
       * @param principalCollection
       * @return
       */
      @Override
      protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
            return null;
      }
      
      /**
       * 用于认证的方法
       * @param authenticationToken
       * @return
       * @throws AuthenticationException
       */
      @Override
      protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
            //获取登录用户名
            String username=authenticationToken.getPrincipal().toString();
            if("tom".equals(username)){
                  //返回一个认证对象，第一个参数为主体，第二个为密码，第三个位realm名称
                  return new SimpleAuthenticationInfo(username,"123",this.getName());
            }
            return null;
      }
}




public class ShiroTest {
      public ShiroTest() {
            //创建安全管理器
            DefaultSecurityManager manager=new DefaultSecurityManager();
            //设置权限认证的数据来源，为类路径下的shiro.ini
            manager.setRealm(new IniRealm("classpath:shiro.ini"));
            //设置安全管理器
            SecurityUtils.setSecurityManager(manager);
            //得到主体
            Subject subject=SecurityUtils.getSubject();
            //产生用户名密码的一个令牌
            UsernamePasswordToken token=new UsernamePasswordToken("tom","123");
            
            subject.login(token);
            System.out.println("登录成功");
      }
      
      public static void main(String[] args) {
            new ShiroTest();
      }
} 
```

## shiro加密

用户密码通常是不会以明文方式存储在数据库。密码需要加密以后，再存储。可以使用md5+salt+散列的方式进行密码加密处理。

MD5信息摘要算法，是一个广泛使用的散列密码加密函数，会产生一个128位（16字节）的散列值，用于确保信息传输完整一致

```

1、定义算法
public class Md5 {
      public static void main(String[] args) {
            //1、真实密码 2、盐 3、散列次数
            Md5Hash md5Hash=new Md5Hash("123","vv",1024);
            按Hex编码
            System.out.println(md5Hash.toHex());
      }
}


2、自定义realm
public class CustomRealm extends AuthorizingRealm {
      public CustomRealm(){
            //创建凭证匹配器
            HashedCredentialsMatcher matcher=new HashedCredentialsMatcher();
            //设置散列次数
            matcher.setHashIterations(1024);
            //设置加密算法
            matcher.setHashAlgorithmName("md5");
            //设置加密编码，用Hex编码，false用Base64
            matcher.setStoredCredentialsHexEncoded(true);
            //将realm设置凭证管理器
            this.setCredentialsMatcher(matcher);
      }
      /**
       * 用于授权的方法
       * @param principalCollection
       * @return
       */
      @Override
      protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
            return null;
      }
      
      /**
       * 用于认证的方法
       * @param authenticationToken
       * @return
       * @throws AuthenticationException
       */
      @Override
      protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
            //获取登录用户名
            String username=authenticationToken.getPrincipal().toString();
            if("tom".equals(username)){
                 //返回一个认证对象，第一个参数为主体，第二个为加密后的密码，第三个参数为盐的字节数组生成的ByteSource、第四个参数realm名称
                  return new SimpleAuthenticationInfo(username,"b7625b3ff35cd11b923a5e24352bb139", ByteSource.Util.bytes("vv".getBytes()),this.getName());
            }
            return null;
      }
}


public class ShiroTest {
      public ShiroTest() {
            //创建安全管理器
            DefaultSecurityManager manager=new DefaultSecurityManager();
            //设置权限认证的数据来源，为类路径下的shiro.ini
            manager.setRealm(new CustomRealm());
            //设置安全管理器
            SecurityUtils.setSecurityManager(manager);
            //得到主体
            Subject subject=SecurityUtils.getSubject();
            //产生用户名密码的一个令牌
            UsernamePasswordToken token=new UsernamePasswordToken("tom","123");
          
            try {
                  //登录认证
                  subject.login(token);
                  System.out.println("登录成功");
            }catch (UnknownAccountException e){
                  System.out.println("用户名错误");
            }catch (IncorrectCredentialsException e){
                  System.out.println("密码错误");
            }
           
      }
      
      public static void main(String[] args) {
            new ShiroTest();
      }
}
```

//认证

```
public class CustomRealm extends AuthorizingRealm {
      public CustomRealm(){
            //创建凭证匹配器
            HashedCredentialsMatcher matcher=new HashedCredentialsMatcher();
            //设置散列次数
            matcher.setHashIterations(1024);
            //设置加密算法
            matcher.setHashAlgorithmName("md5");
            //设置加密编码，用Hex编码，false用Base64
            matcher.setStoredCredentialsHexEncoded(true);
            //将realm设置凭证管理器
            this.setCredentialsMatcher(matcher);
      }
      /**
       * 用于授权的方法
       * @param principalCollection
       * @return
       */
      @Override
      protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
            //得到主体名称，也就是用户名
            String username=principalCollection.getPrimaryPrincipal().toString();
            if ("tom".equals(username)){
                  //创建授权对象
                  SimpleAuthorizationInfo simpleAuthorizationInfo=new SimpleAuthorizationInfo();
                  //授权后用户拥有这些角色对应的权限
                  simpleAuthorizationInfo.addRoles(List.of("custom","manager","order"));
                  //给用户授予指定的权限
                  simpleAuthorizationInfo.addStringPermission("user:create");
                  simpleAuthorizationInfo.addStringPermission("order:*");
                  return simpleAuthorizationInfo;
            }
            return null;
      }
      
      /**
       * 用于认证的方法
       * @param authenticationToken
       * @return
       * @throws AuthenticationException
       */
      @Override
      protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
            //获取登录用户名
            String username=authenticationToken.getPrincipal().toString();
            if("tom".equals(username)){
                  //返回一个认证对象，第一个参数为主体，第二个为加密后的密码，第三个参数为盐的字节数组生成的ByteSource、第四个参数realm名称
                  return new SimpleAuthenticationInfo(username,"b7625b3ff35cd11b923a5e24352bb139", ByteSource.Util.bytes("vv".getBytes()),this.getName());
            }
            return null;
      }
}


public class ShiroTest {
      public ShiroTest() {
            //创建安全管理器
            DefaultSecurityManager manager=new DefaultSecurityManager();
            //设置权限认证的数据来源，为类路径下的shiro.ini
            manager.setRealm(new CustomRealm());
            //设置安全管理器
            SecurityUtils.setSecurityManager(manager);
            //得到主体
            Subject subject=SecurityUtils.getSubject();
            //产生用户名密码的一个令牌
            UsernamePasswordToken token=new UsernamePasswordToken("tom","123");
          
            try {
                  //登录认证
                  subject.login(token);
                  System.out.println("登录成功");
                  //查看当前用户是否有manager这个角色
                  System.out.println(subject.hasRole("manager"));
                  //查看当前用户是否同时具有这两个角色
                  System.out.println(subject.hasAllRoles(List.of("manager","order")));
                  //查看当前用户是否拥有指定权限
                  System.out.println(subject.isPermitted("user:create"));
                  System.out.println(subject.isPermittedAll("user:create","order:add"));
            }catch (UnknownAccountException e){
                  System.out.println("用户名错误");
            }catch (IncorrectCredentialsException e){
                  System.out.println("密码错误");
            }
            
           
      }
      
      public static void main(String[] args) {
            new ShiroTest();
      }
}
```

# SpringBoot整合shiro

1. 导入依赖

   ```
   <dependency>
       <groupId>org.apache.shiro</groupId>
       <artifactId>shiro-core</artifactId>
       <version>1.8.0</version>
   </dependency>
   <dependency>
       <groupId>org.apache.shiro</groupId>
       <artifactId>shiro-spring-boot-starter</artifactId>
       <version>1.8.0</version>
   </dependency>
   ```

2. 自定义real

   ```
   @Component
   public class CustomRealm extends AuthorizingRealm {
         @Autowired
         private IUserService userService;
         public CustomRealm(){
               //凭证管理器
               HashedCredentialsMatcher matcher=new HashedCredentialsMatcher();
               //设置加密算法
               matcher.setHashAlgorithmName("md5");
               //设置散列次数
               matcher.setHashIterations(1024);
               //设置为hex编码
               matcher.setStoredCredentialsHexEncoded(true);
               //设置凭证管理器
               this.setCredentialsMatcher(matcher);
         }
         /**
          * 用于授权的方法
          * @param principalCollection
          * @return
          */
         @Override
         protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
               //得到登录用户
               UserBean userBean= (UserBean) principalCollection.getPrimaryPrincipal();
               //创建授权对象
               SimpleAuthorizationInfo info=new SimpleAuthorizationInfo();
               //添加角色
               userBean.getList().forEach(e->{
                     info.addRole(e.getGradeName());
               });
               userBean.getGradeBeanList().forEach(e->{
                     info.addStringPermission(e.getContent());
               });
               return info;
         }
         
         /**
          * 用于认证的方法
          * @param authenticationToken
          * @return
          * @throws AuthenticationException
          */
         @Override
         protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
         String username=authenticationToken.getPrincipal().toString();
         UserBean userBean=userService.login(username);
         if(userBean!=null){
               return new SimpleAuthenticationInfo(userBean,userBean.getPwd(),
                       ByteSource.Util.bytes(userBean.getSalt().getBytes()),
                       this.getName());
         }
         
               return null;
         }
   }
   ```

3. shiro配置类

   ```
   @Configuration
   public class ShiroConfig {
         
         /**
          * 注册安全管理器
          * @param realm
          * @return
          */
         @Bean
         public DefaultWebSecurityManager defaultSecurityManager(CustomRealm realm){
               DefaultWebSecurityManager defaultSecurityManager=new DefaultWebSecurityManager();
               defaultSecurityManager.setRealm(realm);
               return defaultSecurityManager ;
         }
         @Bean("shiroFilterFactoryBean")
         public ShiroFilterFactoryBean getFilterFactoryBean(DefaultSecurityManager manager){
               ShiroFilterFactoryBean filterFactoryBean=new ShiroFilterFactoryBean();
               //设置安全管理器
               filterFactoryBean.setSecurityManager(manager);
               //认证失败以后，转发的路径
               filterFactoryBean.setLoginUrl("http://localhost:8084/wareLogin");
               //定义拦截链
               Map<String,String> filterMap=new LinkedHashMap<>();
               filterMap.put("/user/login","anon");
               //添加受限资源
               filterMap.put("/**","authc");
               filterFactoryBean.setFilterChainDefinitionMap(filterMap);
               return filterFactoryBean;
         }
   }
   ```

4. 定义全局的异常类对授权失败进行信息输出

   ```
   @RestControllerAdvice
   public class PressException {
         @ExceptionHandler({AuthorizationException.class, UnauthorizedException.class})
         public String noPermission(){
               return "err";
         }
   }
   ```

5. 登录

   ```
   @RequestMapping("login")
         public String login(String name,String pwd){
               //得到主体
               Subject subject=SecurityUtils.getSubject();
               try{
                     subject.login(new UsernamePasswordToken(name,pwd));
                     //登录完成以后shiro会将登录用户绑定在session里面
                     UserBean userBean= (UserBean) subject.getPrincipal();
               }catch (Exception e){
                     e.printStackTrace();
                     return "no";
               }
               return "ok";
         }
   ```

6. 注册用户，给用户密码加密

   ```
   @Override
   public void add(UserBean user) {
         //加盐
         user.setSalt(this.getSalt());
         //加密
         Md5Hash md5Hash=new Md5Hash(user.getPwd(),user.getSalt(),1024);
         user.setPwd(md5Hash.toHex());
         userMapper.insert(user);
   }
   
   /**
    * 获取盐
    * @return
    */
   private String getSalt(){
         String str="abcdefghijklmnopqrstuvwsyzABCDEHGHIJKLMNOPQRSTUVWSYZ@#$";
         StringBuffer sb=new StringBuffer();
         for(int i=0;i<6;i++){
               int index=(int)(Math.random()*str.length());
               sb.append(str.charAt(index));
         }
         return sb.toString();
   }
   ```

7. 添加权限

   ```
   @RestController
   @RequestMapping("/order")
   public class OrderController {
         @RequiresRoles("统计员")
        /* @RequiresRoles({"超级管理员","管理员"})*/
         @RequestMapping("add")
         public String add(){
               return "添加";
         }
         @RequiresPermissions("orde:del")
         @RequestMapping("del")
         public String del(){
               return "删除";
         }
   }
   ```

# shiro缓存

在默认情况下，每次访问受限资源时，都会调用自定义Realm的doGetAuthorizationInfo()方法。并发量大会给数据库造成巨大压力。

一般情况下，用户的权限信息更新并不频繁，

MemoryConstrainedCacheManager是shiro的，减少数据库压力。

# 项目日志记录

项目运行时频繁的使用ststem.out会带来性能的消耗和效率下降，因为System.out是流操作。

System.out显示在控制台，信息很容易丢失，断点调试也只适用于开发阶段，但是在项目运行时，如果抛出异常，无法使用断点调试记录错误信息。

日志可以通过问卷相关配置，通过日志输出级别，什么包下什么类使用什么级别输出，控制输出信息。

log4j

jakarta Log4j日志库，使用一个专门的日志记录包，可以减轻对成千上万的System.out.println语句维护成本，最高版本是1.2.17，在2012年停止更新了，有log4j2代替；

Log4j有三个主要概念

公共类Logger:Logger负责处理日志记录的大部分操作。

公共接口：Appender:Appender负责控制日志记录操作的输出。

公共抽象类：Layout:layout负责格式Appender的动输出

log4j设置了5个级别，不同的级别可以指定的日志是否进行输出。

debug（调试）<info（信息）<warn（警告）<error（错误）<fatal（崩溃）

```
//产生日志对象
            Logger logger= Logger.getLogger(Main.class);
            //定义日志输出格式
            //%C 日志输出类的全路径 %d日志输出日期 %M日志输出方法名 %P日志输出级别
            String pattern="%d{yyyy-MM-dd HH:mm:ss} %C %M [%P] %m %n";
            //输出日志地点为控制台，按正则表达式输出
            Appender appender=new ConsoleAppender(new PatternLayout(pattern));
//            Appender appender=new FileAppender(new PatternLayout(pattern),"log4j/logInfo/LogInfo.txt");
            //在日志添加appender
            logger.addAppender(appender);
            //添加警告级别
            logger.setLevel(Level.WARN);
            //输出日志
            logger.debug("调试信息");
            logger.info("info信息");
            logger.error("错误");
            logger.fatal("崩溃");
```

## 使用配置文件初始化信息

如果记录日志要设置输出信息、级别以及输出地址，很繁琐，所以将指向东西放在配置文件中，可以方便进行管理。

在项目resources目录下加入log4j.properties文件，以描述日志信息。

## log4j.properties

### log4j.rootlogger代表日志输出的基本描述，会应用到所以的日志输出

```
#定义日志输出的基本信息，第一个为参数级别，第二个为日志输出名称
log4j.rootLogger=warn,toConsole
#定义日志输出appender
log4j.appender.toConsole=org.apache.log4j.ConsoleAppender
#定义输出格式为正则表达式
log4j.appender.toConsole.layout=org.apache.log4j.PatternLayout
#日志输出的具体格式
log4j.appender.toConsole.layout.ConversionPattern=%d{yyyy-MM-dd HH:mm:ss} %C %M [%p] %m %n
```

### 可以将指定包中的日志输出应用到某个日志输出名称，文件输出

```
#定义日志输出appender
log4j.logger.com.vv.dao=info,toFile
log4j.appender.toFile=org.apache.log4j.FileAppender
log4j.appender.toFile.layout=org.apache.log4j.PatternLayout
log4j.appender.toFile.layout.ConversionPattern=%d{yyyy-MM-dd HH:mm:ss} %C %M [%p] %m %n
#定义输出文件名
log4j.appender.toFile.File=log4Test/loginInfo/LogInfo.txt
```

### 一天输出一个日志文件

```
log4j.logger.com.vv.service=error,toDayFile
#每天输出一个新文件
log4j.appender.toDayFile=org.apache.log4j.DailyRollingFileAppender
log4j.appender.toDayFile.layout=org.apache.log4j.PatternLayout
log4j.appender.toDayFile.layout.ConversionPattern=%d{yyyy-MM-dd HH:mm:ss} %C %M [%p] %m %n
log4j.appender.toDayFile.DatePattern='.'yyyy-MM-dd
log4j.appender.toDayFile.File=log4Test/dayLog/log.txt
```

当天文件名叫log.txt不是当天文件才会在最后添加.年-月-日

### 输出日志到数据库

```
log4j.logger.com.vv.controller=warn,toDB
#输出日志的地点为数据库
log4j.appender.toDB=org.apache.log4j.jdbc.JDBCAppender

log4j.appender.toDB.layout=org.apache.log4j.PatternLayout
#定义驱动
log4j.appender.toDB.driver=com.mysql.jdbc.Driver
log4j.appender.toDB.URL=jdbc:mysql://localhost:12345/user?characterEncoding=utf8
log4j.appender.toDB.user=root
log4j.appender.toDB.password=txtx
#定义添加日志的正则表达式
log4j.appender.toDB.sql=insert into t_log(l_date,l_content) values('%d{yyyy-MM-dd}','%d{HH:mm:ss} %m')
```

# 日志门面

随着系统的开发和运行，可能会更新不同的日志框架，造成当前日志中不同日志依赖，让我们难以统一控制管理。

所以，我们需要借鉴jdbc的思想，为日志系统也提供一套门面，可以面向日志接口规范来发，而避免依赖具体的日志框架，这样我们系统中就存在日志门面和实现。

常见的日志门面：JCL,slf4j

常见的日志实现：JUL\log4j\logback\log4j2

使用日志门面的优点：

1. 面向接口开发，不再依赖具体的实现类，减少代码耦合。
2. 项目通过导入不同的实现类，可以灵活切换日志框架
3. 统一API，统一配置，便于项目日志管理

简单日志门面slf4j,主要是为了给java日志访问提供统一标准，规范日志API，具体的实现可以交由其他日志框架，如log4j、logback等。另外slf4j也提供了比较简单的实现。

slf4j简单实现：

1. 导入依赖

   ```
   <dependency>
       <groupId>org.slf4j</groupId>
       <artifactId>slf4j-api</artifactId>
       <version>1.7.26</version>
   </dependency>
   <dependency>
       <groupId>org.slf4j</groupId>
       <artifactId>slf4j-simple</artifactId>
       <version>1.7.26</version>
   </dependency>
   ```

2. 在测试类中添加日志输出

   ```
   public class MyTest {
         //产生日志对象
         private Logger logger= LoggerFactory.getLogger(MyTest.class);
         public MyTest() {
               logger.trace("trace追踪");
               logger.debug("dubug调试");
               logger.info("info信息");
               logger.warn("warn警告");
               logger.error("error错误");
         }
         
         public static void main(String[] args) {
               new MyTest();
         }
   }
   ```

   Slf4j中没有fatal级别，新增了trace追踪级别，该级别低于debug

   ## slf4j绑定日志框架：

   slf4j支持各种日志框架，在项目中可以进行统一的日志记录，使用时，需要绑定具体的日志框架，但是slf4j只能绑定一个日志实现框架，如果有多个日志实现框架，那么会使用第一个依赖日志实现框架。

   绑定日志框架需要注意：

   1. 绑定遵循slf4j规范的日志框架（logback\slf4jSimple）只需要添加依赖。
   2. 绑定没有遵循slf4j的框架(log4j\log4j2)时，不但需要添加框架依赖还需要添加日志框架适配器。

## slf4j绑定logback

```
用//导入依赖，注意顺序，这个只会用第一个实现框架ch.qos.logback
<dependency>
    <groupId>org.slf4j</groupId>
    <artifactId>slf4j-api</artifactId>
    <version>1.7.26</version>
</dependency>
<dependency>
    <groupId>ch.qos.logback</groupId>
    <artifactId>logback-classic</artifactId>
    <version>1.2.3</version>
</dependency>
<dependency>
    <groupId>org.slf4j</groupId>
    <artifactId>slf4j-simple</artifactId>
    <version>1.7.26</version>
</dependency>
```

## Slf4j绑定log4j

绑定没有遵循slf4j的框时，不但需要添加框架依赖还需要添加日志框架适配器。

```
<dependency>
    <groupId>log4j</groupId>
    <artifactId>log4j</artifactId>
    <version>1.2.17</version>
</dependency>
<dependency>
    <groupId>org.slf4j</groupId>
    <artifactId>slf4j-log4j12</artifactId>
    <version>1.7.12</version>
</dependency>
```

## logback

logback是由log4j创始人设计的另一款开源日志组件，性能比log4j好。

由于日志都是通过slf4j门面实现的，所以没有级别区别，都是通过修改依赖和配置文件来绑定日志实现框架来实现的。

logback通过配置文件，配置日志的输出，在resources目录下新建logback.xml文件。

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<configuration>
    <!--定义属性-->
    <property name="pattern" value="%d{yyyy-MM-dd} %C [%M] %p ---- %m%n"></property>
    
    
    <!--定义appender，日志输出地址和格式-->
    <appender name="console" class="ch.qos.logback.core.ConsoleAppender">
       <!-- 安装正则表达式输出日志-->
        <encoder class="ch.qos.logback.classic.encoder.PatternLayoutEncoder">
            <pattern>${pattern}</pattern>
        </encoder>
    </appender>
    
    
    
    <!--输出到文件-->
    <appender name="file" class="ch.qos.logback.core.FileAppender">
        <file>logback/file/log.txt</file>
        <encoder class="ch.qos.logback.classic.encoder.PatternLayoutEncoder">
            <pattern>${pattern}</pattern>
        </encoder>
        <!--单独给appender增加级别，输出日志，否则拦截日志-->
        <filter class="ch.qos.logback.classic.filter.ThresholdFilter">
            <level>info</level>
            <!--大于或等于该级别，输出日志，否则拦截日志-->
           <!-- <onMatch>ACCEPT</onMatch>
            <onMismatch>DENY</onMismatch>-->
        </filter>
    </appender>
    
    
    
    <!--一天输出一个日志文件-->
    <appender name="rollFile" class="ch.qos.logback.core.rolling.RollingFileAppender">
        <encoder class="ch.qos.logback.classic.encoder.PatternLayoutEncoder">
            <pattern>${pattern}</pattern>
        </encoder>
        <!--定义拆分文件的规则-->
        <rollingPolicy class="ch.qos.logback.core.rolling.TimeBasedRollingPolicy">
            <fileNamePattern>
                    logback/dayLog/%d{yyyy-MM-dd}.log
            </fileNamePattern>
        </rollingPolicy>
    </appender>
    
    
    <!--定义基本日志输出，全局有效-->
    <root level="All">
        <appender-ref ref="console"></appender-ref>
        <appender-ref ref="file"></appender-ref>
    </root>
    
    
    <!--在指定包装中日志输出采用规则，level表示日志输出级别，additivity表示十分继承roolLogger中的内容-->
    <logger name="com.vv.service" level="warn" additivity="true">
        <appender-ref ref="rollFile"></appender-ref>
    </logger>
</configuration>
```

## log4j2介绍

Apache Log4j的升级版，参考了logback的一些优秀设计，并带来了重大的提升，

提升主要有：

1. 性能提升。log4j2相较于log4j和logback有明显的性能提升。
2. 自动重载配置，在开发阶段修改配置文件不需要重启应用。
3. 无垃圾机制，log4j2大部分情况下采用的是无垃圾机制，避免导致频繁日志收件导致jvm GC

- 官网：https://logging.apache.org/log4j/2.x/index.html

## log4j2开发流程

创建log4j2项目或模块时需要注意，不能使用springboot，因为springboot自带了logback框架，会优先使用logback。

1. 导入依赖

   ```
   <dependency>
       <groupId>org.apache.logging.log4j</groupId>
       <artifactId>log4j-api</artifactId>
       <version>2.11.1</version>
   </dependency>
   <dependency>
       <groupId>org.apache.logging.log4j</groupId>
       <artifactId>log4j-core</artifactId>
       <version>2.11.1</version>
   </dependency>
   ```

2. 在resources目录下添加log4j2.xml配置文件

   ```xml
   <?xml version="1.0" encoding="utf-8" ?>
   <configuration status="WARN" monitorInterval="10">
       <properties>
           <property name="pattern">-------%d{yyyy-MM-dd HH:mm:ss} %C [%p] %M %m%n"</property>
       </properties>
       <!--status表示日志框架本身所用的日志级别，monitorInterval表示自动装配配置文件的时间间隔-->
       <appenders>
           <!--在控制台输出日志-->
           <console name="Console" target="SYSTEM_OUT">
               <!--日志输出格式-->
               <PatternLayout pattern="${pattern}"/>
           </console>
           <!--在文件输出-->
           <File name="file" fileName="log/log.log">
               <!--日志输出格式-->
               <PatternLayout pattern="${pattern}"/>
           </File>
          <!-- 一天一个日志-->
           <RollingFile name="rolling" filePattern="log/%d{yyyy-MM-dd}--%i.log">
              <!-- 日志过滤器-->
               <ThresholdFilter level="trace"></ThresholdFilter>
               <PatternLayout pattern="${pattern}"></PatternLayout>
               <Policies>
                   <!--按时间拆分-->
                   <TimeBasedTriggeringPolicy/>
                   <!--按大小拆分-->
                   <SizeBasedTriggeringPolicy size="100 KB"/>
               </Policies>
           </RollingFile>
       </appenders>
       <loggers>
           <root level="All">
               <appender-ref ref="Console"/>
               <appender-ref ref="file"/>
               <appender-ref ref="rolling"/>
           </root>
       </loggers>
   </configuration>
   ```

   ## springboot整合log4j2

   springboot默认使用logback作为日志实现框架，如果要使用log4j2,那么，需要将springboot原来的日志依赖先屏蔽，然后在导入log4j2的启动器。

   ```
   <dependency>
       <groupId>org.springframework.boot</groupId>
       <artifactId>spring-boot-starter</artifactId>
       <!-- 去掉spring boot自带的日志启动器-->
       <exclusions>
           <exclusion>
               <groupId>org.springframework.boot</groupId>
               <artifactId>spring-boot-starter-logging</artifactId>
           </exclusion>
       </exclusions>
   </dependency>
       <dependency>
           <groupId>org.springframework.boot</groupId>
           <artifactId>spring-boot-starter-log4j2</artifactId>
       </dependency>
   ```

导入log4j2启动后，需要在resources目录下，添加log4j2.xml相关配置文件，指定日志输出的相关配置。

# Websocket

websocket协议是基于TCP的一种新的网络协议。他实现了浏览器与客户端全双工通信，允许服务器主动发消息给客户端。

http是半双工的协议，在同一个请求中客户端向服务器发送请求的时候，服务器是没有数据发送给客户端的，同样服务器向客户端产生响应的时候，客户端也不能向服务器发送请求。半双工效率低，特别是服务器和客户端频繁进行数据交换的时候。

通信只能由客户端发起，如果客户端不发送请求，服务器不会产生响应，也就是服务器不会主动给客户端发送数据，如果服务器的数据发生变化，客户端不能即时更新。

采取http协议进行通信，客户端为了即时得到服务器的数据的变化，通常采用轮询的方式。也就是在客户端做一个时间函数，每隔一段时间向服务器发送一次请求，以得到服务器的最新数据。即使服务器的数据没有变化，这样的请求响应也会隔一段时间发生一次。

轮询方式和长连接一样都非常占用服务器资源，客户端不断的等待服务器数据返回，服务器的压力增大导致延时很严重。

### websocket特点：

websocket客户端和服务器只需要一次握手，就可以建立服务器与客户端之间的连接，并实现双向数据连接。

1. 建立在TCP协议之上，服务器端实现比较容易
2. 与http兼容，默认端口也使用80端口和443，并且握手阶段也采用http协议，因此握手也不容易被屏蔽，能通过各种http代理的服务器。
3. 数据格式比较轻量，性能开销小，通信高效。
4. 可以发送文本也可以发送二进制数据。
5. 没有同源、跨域限制，客户端与服务器可以随意通信。

## Websocket通信原理

websocket是基于http协议，先由客户端发送http请求，但建立升级协议，询问服务器是否同意。

![img](https://static.dingtalk.com/media/lQLPJxzuxdIXFSjNAZzNA7Wwh0oT3gOciiUEEnIBksCmAA_949_412.png_720x720q90g.jpg?bizType=im)

如果服务器支持并同意，会在响应中返回支持并同意某种协议。

![img](https://static.dingtalk.com/media/lQLPJwmtT9F2FajNAT_NBH2wl1_xAWKgKXQEEnJIvkAqAA_1149_319.png_720x720q90g.jpg?bizType=im)

## websocket开发

1. 导入依赖

   ```xml
   <dependency>
       <groupId>org.springframework.boot</groupId>
       <artifactId>spring-boot-starter-websocket</artifactId>
   
   </dependency>
   ```

2. 创建一个服务器端的配置类

   ```
   @Configuration
   public class WebsocketConfig {
         @Bean
         public ServerEndpointExporter getServerEndpointExporter(){
               return new ServerEndpointExporter();
         }
   }
   ```

3. 创建服务器端的websocket处理器

   ```java
   /**
    * websocket服务器
    */
   @ServerEndpoint("/socketInfo")
   @Component
   public class WebsocketServer {
         private static List<Session> sessionList=new ArrayList<>();
         /**
          * 客户端和服务器建立连接时触发
          */
         @OnOpen
         public void onOpen(Session s) {
               System.out.println("websocket建立连接");
               sessionList.add(s);
         }
         /**
          * 客户端和服务器断开连接时触发
          */
         @OnClose
         public void onClose(Session s) {
               System.out.println("连接关闭");
               sessionList.remove(s);
         }
         /**
          * 客户端向服务器发送消息时触发
          */
         @OnMessage
         public void onMessage(String msg,Session s) throws IOException {
               //向每个建立连接的客户端推送消息
               for(Session session:sessionList){
                     session.getBasicRemote().sendText(msg);
               }
         }
         /**
          * 发生异常时
          */
         @OnError
         public void onError(Throwable e) {
         }
   }
   ```

4. 在客户端创建websocket对象

   ```html
   <html lang="en">
   <head>
      <meta charset="UTF-8">
      <title>Title</title>
   </head>
   <style>
      #infoDiv{
         width: 300px;
         height: 300px;
         border: 1px pink solid;
      }
   </style>
   <script>
      function $(id){
         return document.getElementById(id);
      }
      let websocketObj;
      window.onload=function (){
         socketInit();
      }
      function socketInit(){
         //创建websocket对象
         websocketObj=new WebSocket("ws://127.0.0.1:8080/socketInfo");
          //建立连接时触发，websocketObj.readyState准备状态
          //状态分为4中：
          //0---实例化websocket完成，正在创建连接；
          //1---连接成功建立；
          //2---连接正在关闭；
          //3---连接已经关闭
         websocketObj.onopen=function (op){
            console.log(websocketObj.readyState);//准备状态
         }
         websocketObj.onclose=function (){}
         websocketObj.onerror=function (){}
         websocketObj.onmessage=function (back){
            $("infoDiv").innerHTML+=back.data+"<br>";
         }
      }
      function sendMag(){
          //客户端向服务器发送信息
         websocketObj.send($("msg").value)
         $("msg").value="";
      }
   </script>
   <body>
   <div id="infoDiv"></div>
   <input type="text" id="msg">
   <input type="bulue="发送消息" onclick="sendMag()">
   </body>
   </html>
   ```