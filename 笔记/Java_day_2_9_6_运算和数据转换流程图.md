# Java_day_2_9_6

## 三目运算

```java
int b=19;
boolean bb=b>18 ? true:false;
```

## 逻辑

与、或、非

- 与 &&
- 或 ||
- 非  ！

## 数据类型转换

- 自动转换（大数据方向进行转换例如int 4字节double 8字节,就会往double转换，也称为向上转型）
- 强制转换 `int num=(int)3.14 `

## 输入

```java
 		System.out.println("请输入一个4位数字：");
        Scanner sc=new Scanner(System.in);
        int sum=sc.nextInt();
        System.out.println(sum/1000);
        System.out.println(sum%1000/100);
        System.out.println(sum%100/10);
        System.out.println(sum%10);
```

## 流程控制

1. 顺序结构
2. 选择结构（判读）
3. 循环结构（反复）

程序语句控制程序的走向

![image-20220906171027682](C:\Users\60952\AppData\Roaming\Typora\typora-user-images\image-20220906171027682.png)

## 判读

if

```java
 if(条件){
                
            }
```

判读偶数和闰年

```java
		Scanner sc=new Scanner(System.in);
 		System.out.println("请输入一个数字：");
        int num=sc.nextInt();
        if(num%2==0){
            System.out.println("是偶数");
        }else{
            System.out.println("不是偶数");
        }
        System.out.println("请输入一个年份：");
        int year=sc.nextInt();
        if(year%4==0&&year%100!=0||year%400==0){
            System.out.println("是闰年");
        }else{
            System.out.println("不是闰年");
        }
```



