## final

`public final`

final修饰的方法，叫最终方法，子类不能重写。

final修饰的类叫最终类，终态类，不能被继承。

## 继承层次结构

java是单继承，只有一个父类。

多继承的好处丰富度高，接收的功能更多，坏处容易造成功能冗余。

单继承好处结构清晰、简单，坏处欠缺丰富度。

## 多态

面向对象四大特征：封装、继承、多态、抽象

```java
public class Animal {
    public void eat() {
        System.out.println("吃饭");
    }
}
public class Dog extends Animal {
    public void eat() {
        System.out.println("小狗吃饭");
    }
}
public class Test {
    public static void main(String[] args) {
        Animal d = new Dog();//多态的表现方式
        d.eat();
    }
}
```

多态的实现：

1. 继承
2. 方法重写
3. 父类的类型指向子类的类型

转型：向上转型和向下转型

```java
double a = 5;//向上转型
int b=(int)(Math.random());//向下转型
```

uml类图：父类>子类>派生类（父类和子类转型只能向上转型，不能向下转型。）