## 构造器

特性：

1. 当开发人员没有在类里主动书写构造器，自动生成一个公共无参构造函数
2. 开发人员书写构造器
   - 构造器名字跟类名一致
   - 构造器没有返回类型，void都没有
   - 访问修饰符、参数列表根据需要

## 关系：

1. 有 `has-a` 关系 指一个对象内部 “拥有” 另一个对象

   ```java
   public class Cat {
   	public String name;
   	public Fish f;
   }
   ```

   

2. 用 `use-a` 关系 指一个对象内部 “使用” 另一个对象

   ```java 
   
   public class Cat {
   	public String name;
       public void eat(Fish f) {
           System.out.println(this.name + "爱吃" + f.kind);
       }
   }
   ```

   

3. 是 `is-a` 关系 指一个对象是以另一个对象的特例，继承

# 包



当程序越来越大，书写的类越来越多，文件们就需要一种结构能够进行存放和管理。于是就有了包的概念去分门别类地管理多个类。包在本质上就是用来专门管理` java 类`的文件夹。

创建包的方式：对 `src `文件夹右键 - 新建 - 软件包。包的命名规范：全小写。



当一个 `java 类`被放入到某个包中，有以下特点：



1. 在该类的最高处要增加一句包声明的代码 package 包名; 例如 package traffic;
2. 默认情况下，一个` java 类`只能访问来自于同包的其他` java 类`，如果需要访问其他包的类，需要导入 import 将那个类导入进来。就好比以前导入 Scanner 一样` import java.util.Scanner; `可以使用 import 包名.* 导入某个包下所有的类，但不建议这样做，尽量用哪些导哪些
3. IDEA 工具中有多个创建包和创建类的方式



通常会在一个包中放所有 main() 方法的` java 类`，再在其他包中放各种类。

## 总结：

我们今天学习了构造器，通过练习知道了默认的构造器是无参构造器，无参有参构造器的区别就是（）有无参数，可以同时存在，如果只有有参构造函数，那么默认的就会失效；在写类与类的关系的时候，有3种创建关系的方法，其中在用use-a关系的时候要考虑是不是一定需要，就比如练习战士使用武器击败战士的时候，开始的时候是只写了`public void attack(Soldier s, Weapon weapon){}`这一个方法，在老师的提示下，突然悟了，我们应该还要考虑战士不用武器的情况，可以用方法的重载。

```java
public void attack(Soldier s){}//不用武器
public void attack(Soldier s, Weapon weapon){}//用武器
```



