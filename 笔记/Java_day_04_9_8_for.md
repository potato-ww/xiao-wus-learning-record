# Java_9_8

## `for`介绍

```java
for(int i=0;i<3;i++){
    System.out.println(i)
}
1.表达式1初始化
2.表达式2范围
3.表达式3自增或自减
```

执行顺序：

- 执行1执行一次
- 表达式2 true
- 循环内容
- 表达式3自增或自减
- 2比较true
- 循环内容
- 表达式3自增或自减
- .....
- 结束

<u>一定要写对判断条件（表达式2），不然就会出错。</u>

## for相关的题

判断是不是质数（质数开始没想明白）

```java
		Scanner sc = new Scanner(System.in);
        System.out.println("请输入一个数：");
        int num = sc.nextInt();
        boolean br = true;
        for (int i = 2; i < num; i++) {
            if (num % i == 0) {
                br = false;
            }
        }
        if (br && num != 1) {
            System.out.println(num + "是质数");
        } else {
            System.out.println(num + "不是质数");
        }
    }
```

【打印前 20 项非波拉契数列】1,1,2,3,5,8,13,21,34等等。(常见题)

```java
		int a = 1, b = 1, sum = 0;
        for (int i = 0; i <= 19; i++) {
            a = b;
            b = sum;
            sum = a + b;
            System.out.print(sum + ",");
        }
```

总结：通过不断的做for循环的题，已经把for循环的工作原理熟记于心。在写逻辑代码时可以先画草图帮助理解；除此之外，写过的笔记要经常回顾，不然隔太久容易忘记。