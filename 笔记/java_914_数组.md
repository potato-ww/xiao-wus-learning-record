## 数组

数组属于引用类型。

## 遍历 for each

遍历数组除了可以通过 for 取下标，还可以通过 for each，直接拿到数组中每一个元素

```java
for (int item : 数组名字) {
    System.out.println(item);
}

for (int item : newArr) {
    System.out.println(item);
}
```

## 例题：

```java
 /* 数组版ATM*/
        String[] users = {"zhangsan", "lisi"};
        String[] passwords = {"123", "456"};
        int[] money = {500, 1000};
        int num = 0;//下标
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入您的用户名和密码");
        String username = sc.next();
        String password = sc.next();
        boolean flg = true;//判断登录是否有效
        boolean brIn = false;//判断登录是否成功
        for (int i = 0; i < users.length; i++) {
            if (users[i].equals(username) && 				passwords[i].equals(password)) {
                System.out.println(users[i] + "欢迎您！");
                flg = false;//登录有效
                brIn = true;//操作atm
                num = i;
                break;
            }
        }
        if (flg) {
            System.out.println("输入有误");
        }
        while (brIn) {
            System.out.println("请选择您的操作：1、存钱 2、取钱 3、查询 4、退出");
            int handle = sc.nextInt();
            switch (handle) {
                case 1:
                    System.out.println("请输入您要存入的金额：");
                    double addMoney = sc.nextDouble();
                    money[num] += addMoney;
                    System.out.println("您的余额：" + money[num]);
                    break;
                case 2:
                    System.out.println("请输入您要取出的金额：");
                    double lossMoney = sc.nextDouble();
                    if (lossMoney <= money[num]) {
                        money[num] -= lossMoney;
                        System.out.println("取款成功！您的余额：" + money[num]);
                    } else {
                        System.out.println("余额不足");
                    }
                    break;
                case 3:
                    System.out.println("您的余额：" + money[num]);
                    break;
                case 4:
                    System.out.println("退出成功");
                    brIn = false;//退出操作Atm
                    break;
                default:
                    System.out.println("错误操作！");
                    break;
            }

        }
```

在做数组ATM这个作业的时候，我们要考虑到有许多人的情况，所以不能写死，然后我又在登录以后写了for循环，这样就避免了写死，但是我没有考虑到登录成功了就不用再判断是谁了，最后参考了一位同学的，焕然大悟，我只想着可以让他们下标一致，却忽略了在登录的时候可以用一个变量把下标记录一下，这样登录成功以后就不用再获取下标了。

```java
//打印1-10里6个不重复的数
 int[] arr = new int[6];
        for (int i = 0; i < arr.length; i++) {
            boolean br = false;
            int num = (int) (Math.random() * 10 + 1);
            for (int j = 0; j < i; j++) {
                if (num == arr[j]) {
                    br = true;
                }
            }
            if (br) {
                i--;
            } else {
                arr[i] = num;
            }
        }
        for (int item : arr) {
            System.out.print(item + " ");
        }
```

最开始这个题一直在纠结，如何解决有相同的值的时候会有一个新的值而不是直接不赋值为0，在反复修改以后，可以用i--实现遇到重复值就返回上一次循环，重新赋值比较，这样就解决了问题，当前这个值后面的比较；有无重复的是和当前值之前的比较。

```java
//字符串转数组,找出a的个数
        String str = "wbnmzaxc";
        char[] arr = str.toCharArray();
        int count = 0;
        for (int i = 0; i < arr.length; i++) {
            if(arr[i]=='a'){
                count++;
            }
        }
        System.out.println(count);
```

```java
//判断连续ab的个数
        String str = "abbbabcdab";
        char[] arr = str.toCharArray();
        int count = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == 'a' && arr[i + 1] == 'b') {
                count++;
            }
        }
        System.out.println(count);
```

