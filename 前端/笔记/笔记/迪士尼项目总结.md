# 迪士尼项目总结

1. 清空项目全部的margin和padding

   ```css
   *{
       margin: 0;
       padding: 0;
   }
   ```

   

2. 首先分析页面类型，让页面居中

   ```css
   .content{
       width: 1200px;
       margin: 0 auto;
   }
   ```

   

3. 查看页面布局效果可以给当前板块加一个背景颜色`background-color: cadetblue;`

4. 去掉li的样式`list-style: none;`

5. 一般头部用`header`里面加一个`nav`,然后里面用一个`UL`

6. 导航栏一般格式

   ```html
   /*使所有的li到一行*/
   header ul{
       display: flex;
       margin-top: 20px;
   }
   li{
       list-style: none;
   }
   /*平均分配空间，使li文字居中*/
   header ul li{
       flex-grow: 1;
       text-align: center;
   }
   header ul li a{
       /* 去下划线 */
       text-decoration: none;
       color: black;
   }
   header ul li a:hover{
       color: red;
   }
   
   <header>
               <nav>
                   <ul>
                       <li>
                           <a href="#">首页</a>
                       </li>
                   </ul>
               </nav>
           </header>
   ```

   

7. 拿到图纸要先划分好整体空间，写一点检查一点，要习惯性注解，要学会使用浏览器的检查功能。

# 项目编写回顾

1. 观察整体布局，此页面为一个整体居中布局：

   ```html
   .content{
           width: 1000px;
           margin: auto;
       }
   <div class="content"></div>
   ```

   

2. 首先编写头部用`header>nav>ul>li`结构嵌套，在`ul`里面设置`display:flex;`属性使li横向排列，在`li`运用平均分配`flex-grow:1`和`text-align:center`,z在修改一些细节样式就可以达到想要的效果。

3. 导航栏下面是一个`img`

4. 明星部分，首先发现 明星 商店标题样式一个，可以同一样式代码编写，明星部分采用的是`div>ul>li>img p`结构嵌套，排列方法与导航栏类似。

5. 商店部分与资讯部分结构类似，只需要写一个部分，另一部分复制，然后修改一些细节；商店部分，除开标题，主体部分可分为左右两部分，左边是一个`img`,右边是4个`div`里面嵌入`img`,大概样式设置：

   ```css
   .shop .layout .layoutRight{
       width: 50%;
       height: 464px;
       display: flex;
       flex-wrap: wrap;
       /* background-color: rgb(17, 139, 239); */
   }
   ```

   就可以得到大概布局，然后细节修改。

6. 资讯部分与商店类似可复制修改。

7. 尾部用`footer`大概可分为上下两部，上部结构为：

   ```html
   				<div class="item_1">
                       <img src="../test/disney.cn/b_logo.png" alt="">
                   </div>
                   <div class="item_2">
                       <img src="../test/disney.cn/weibo.jpg" alt="">
                       <p>关注我们微博</p>
                   </div>
                   <div class="item_2">
                       <img src="../test/disney.cn/weibo.jpg" alt="">
                       <p>关注我们微信</p>
                   </div>  
   
   
   ```

   下部分结构为

   ```html
   				<ul>
                       <li><a href="#">关于我们</a></li>
                       <li><a href="#">加入我们</a></li>
                       <li><a href="#">法律条款</a></li>
                       <li><a href="#">隐私政策</a></li>
                       <li><a href="#">退换货政策</a></li>
                   </ul>
                   <p>
                       Disney I Pixar All rights reseverd. 
                       <span>备案号：泸</span>
                       B2-20040339-3
                   </p>
   ```

   属性设置与导航栏类似。