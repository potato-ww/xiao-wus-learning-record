package polymorphic.gun;


import java.util.Scanner;

public class Test {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("请问你要使用什么枪？");
        String gun = sc.next();
        Solider s = new Solider();
        switch (gun) {
            case "手枪":
                Gun g = new Pistol();
                s.shoot(g);
                break;
            case "机枪":
                Gun g1 = new MachineGun();
                s.shoot(g1);
                break;
            case "步枪":
                Gun g2 = new Rufle();
                s.shoot(g2);
                break;
            default:
                System.out.println("输入有误！！");
                break;
        }

    }
}
