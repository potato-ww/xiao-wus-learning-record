package polymorphic.gun;

/*根据使用的枪 Gun 的不同有不同的声音 voice
        •  手枪 Pistol “手枪 biu！“
        •  步枪 Rufle “步枪 pa！”
        •  机枪 MachineGun "机枪 dadadadadada！”*/
public class Gun {
    public void voice() {//声音
        System.out.println("有枪声");
    }
}
