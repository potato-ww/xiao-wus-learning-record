package polymorphic.gun;

/*【士兵 Solider 都有射击 shoot 的行为，根据使用的枪 Gun 的不同有不同的声音 voice
        •  手枪 Pistol “手枪 biu！“
        •  步枪 Rufle “步枪 pa！”
        •  机枪 MachineGun "机枪 dadadadadada！”*/
public class Solider {
    public void shoot(Gun g) {//引用枪类
        g.voice();
    }
}
