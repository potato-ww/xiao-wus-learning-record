package inherit.botany;

public class Tree extends Botany {
    private String gender;

    public Tree(String name, int age, String gender) {
        super(name, age);
        this.gender = gender;
    }


    public Tree() {
    }

    public void pickTheFruit() {
        System.out.println("这是一棵" + getAge() + "岁" + getGender() + getName());
    }

    public void show() {
        super.show();
        System.out.println("子show");
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }
}
