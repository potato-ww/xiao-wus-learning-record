package inherit.botany;

public class Botany {
    private String name;
    private int age;

    public Botany(String name, int age) {
        this.name = name;
        this.age = age;
        System.out.println("父");
    }

    public Botany() {
    }

    public void makingOxygen() {
        System.out.println("制造氧气");
    }

    public void show() {
        System.out.println("父show");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

}
