package inherit.graph;

public class Trangle extends Graph {//三角形
    private int d;//低
    private int h;//高

    public Trangle(int d, int h) {
        this.d = d;
        this.h = h;
    }

    public Trangle() {
    }

    public double getArea() {//面积
        return (double) this.d * this.h / 2;
    }

    public int getD() {
        return d;
    }

    public void setD(int d) {
        this.d = d;
    }

    public int getH() {
        return h;
    }

    public void setH(int h) {
        this.h = h;
    }
}
