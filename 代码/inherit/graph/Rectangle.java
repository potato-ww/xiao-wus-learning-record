package inherit.graph;

public class Rectangle extends Graph {//矩形


    public Rectangle() {
    }

    public Rectangle(int width, int height) {
        super(width, height);
    }

    public double perimeter() {
        return (getWidth() * 2) + (getHeight() * 2);//计算周长
    }

    public double getArea() {//面积
        return getHeight() * getWidth();
    }
}
