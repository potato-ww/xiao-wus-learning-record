package inherit.graph;

import java.util.Scanner;

public class Test {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        boolean fig = true;
        while (fig) {
            System.out.println("请输入：1、计算三角形的面积 2、计算圆的面积和周长请 3、计算矩形的面积和周长 4、正方形的面积和周长 5、退出");
            int num = sc.nextInt();//判断选择的是什么
            switch (num) {
                case 1://三角形
                    Trangle trangle = new Trangle();
                    System.out.println("请输入底和高：");
                    trangle.setD(sc.nextInt()); //赋值
                    trangle.setH(sc.nextInt());
                    trangle.setBgColor("黄色");
                    double s = trangle.getArea();//接收返回值
                    System.out.println(trangle.getBgColor() + "三角形的面积为：" + s);
                    break;
                case 2://圆
                    Circle circle = new Circle();
                    System.out.println("请输入半径：");
                    circle.setR(sc.nextInt());//赋值
                    circle.setBgColor("绿色");
                    double s2 = circle.getArea();//接收返回值
                    double l2 = circle.perimeter();
                    System.out.println(circle.getBgColor() + "圆的面积为：" + s2 + ",周长:" + l2);
                    break;
                case 3://矩形
                    Rectangle rectangle = new Rectangle();
                    System.out.println("请输入宽和高：");
                    rectangle.setWidth(sc.nextInt());//赋值
                    rectangle.setHeight(sc.nextInt());
                    rectangle.setBgColor("红色");
                    double s3 = rectangle.getArea();//接收返回值
                    double l3 = rectangle.perimeter();
                    System.out.println(rectangle.getBgColor() + "矩形的面积为：" + s3 + ",周长:" + l3);
                    break;
                case 4://正方形
                    Square square = new Square();
                    System.out.println("请输入边长：");
                    int l = sc.nextInt();//正方形边长
                    square.setWidth(l);//赋值
                    square.setHeight(l);
                    square.setBgColor("蓝色");
                    double s4 = square.getArea();//接收返回值
                    double l4 = square.perimeter();
                    System.out.println(square.getBgColor() + "正方形形的面积为：" + s4 + ",周长:" + l4);
                    break;
                case 5://退出
                    System.out.println("退出成功！");
                    fig = false;
                    break;
                default:
                    System.out.println("错误输入！请重新输入：");
                    break;
            }
        }
    }

}
