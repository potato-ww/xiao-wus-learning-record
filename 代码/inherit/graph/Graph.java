package inherit.graph;

public class Graph {
    /*让用户选择，计算图形面积、周长*/
    private int width;//宽
    private int height;//高
    private String bgColor;//背景颜色

    public Graph() {
    }

    public Graph(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public double getArea() {//面积
        return 0;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public String getBgColor() {
        return bgColor;
    }

    public void setBgColor(String bgColor) {
        this.bgColor = bgColor;
    }
}
