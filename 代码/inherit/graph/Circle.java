package inherit.graph;

public class Circle extends Graph {//园
    private int r;//半径

    public double getArea() {
        return Math.PI * this.r * this.r;
    }

    public double perimeter() {//周长
        return 2 * Math.PI * this.r;
    }

    public int getR() {
        return r;
    }

    public void setR(int r) {
        this.r = r;
    }
}
