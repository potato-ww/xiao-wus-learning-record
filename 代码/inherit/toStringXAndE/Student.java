package inherit.toStringXAndE;

public class Student {
    private String name;
    private int grade;

    public Student() {
    }

    public Student(String name, int grade) {
        this.name = name;
        this.grade = grade;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", grade=" + grade +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;//如果实例名一样
        if (o == null || getClass() != o.getClass()) return false;//如果内容为null,或者类型不一样

        Student student = (Student) o;

        if (grade != student.grade) return false;//grade不相同
        return name.equals(student.name);//name不同
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getGrade() {
        return grade;
    }

    public void setGrade(int grade) {
        this.grade = grade;
    }
}
