package classes.packaging;

import java.util.Scanner;

public class Test {
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("请问你要输入几位同学的成绩");
        int num = sc.nextInt();
        Students[] arr = new Students[num];//创建学生数组
        for (int i = 0; i < arr.length; i++) {
            System.out.println("请输入姓名和成绩");
            String name = sc.next();//接收姓名
            double cj = sc.nextDouble();//接收成绩
            if (cj >= 0) {//成绩是否合理
                Students s = new Students(name, cj);//创建学生对象
                arr[i] = s;//成绩传入数组
            } else {//不合理
                System.out.println("输入有误,请重新输入");
                i--;
            }
        }
        set(arr);

    }


    public static void set(Students[] arr) {//排序
        for (int i = 0; i < arr.length; i++) {
            for (int j = i + 1; j < arr.length; j++) {
                if (arr[i].getGrade() < arr[j].getGrade()) {
                    Students num = new Students();//创建一个学生容器
                    num = arr[i];//将当前值存入容器
                    arr[i] = arr[j];
                    arr[j] = num;
                }
            }
        }
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i].getName() + " ");
        }
    }
}
