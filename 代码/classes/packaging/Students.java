package classes.packaging;

/*书写一个 Student 类包含学生的姓名、成绩，再创建5个学生输入成绩，分数排序*/
public class Students {
    private String name;
    private double grade;

    public Students() {
    }

    public Students(String name, double grade) {
        this.name = name;
        this.grade = grade;
    }

    public Students(double grade) {
        this.grade = grade;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getGrade() {
        return grade;
    }

    public void setGrade(int grade) {
        this.grade = grade;
    }
}
