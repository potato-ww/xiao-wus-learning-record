package classes.game;

public class Weapon {
    /*游戏中还有武器 Weapon，武器拥有名字 name 、攻击力 atk；
    如果战士角色拥有武器，那么它的攻击力就要叠加上武器的攻击力，从而计算对方的失血量*/
    public String name;
    public double atk;

    public Weapon() {
    }

    public Weapon(String name, double atk) {
        this.name = name;
        this.atk = atk;
    }
}
