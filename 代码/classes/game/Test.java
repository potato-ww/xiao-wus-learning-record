package classes.game;

public class Test {
    public static void main(String[] args) {
        Weapon weapon1 = new Weapon("斧头", 500);
        Weapon weapon2 = new Weapon("砍刀", 400);
        Soldier s1 = new Soldier("夏侯惇", 20000, 2000, 2000);//英雄1
        Soldier s2 = new Soldier("曹操", 10000, 1000, 1000);//英雄2
        s1.attack(s2, weapon1);
        s1.attack(s2);
    }
}
