package classes.game;

public class Soldier {
    /*设计一个游戏系统，在该系统当中拥有战士 Soldier 角色； 每个角色拥有自己的名字 name 、
    等级 level、生命力 hp ，攻击力 atk、防御力 def；
    战士都有 普通攻击 行为 attack，其实现为：根据自己的攻击力减去 被攻击对象的防御力
    从而得到对方的失血量；

    设计这个系统中有哪些类、分别有什么属性、行为，有什么关联关系，并在main方法中调用查看效果。*/
    public String name;//姓名
    public String level;//等级
    public double hp;//生命力
    public double atk;//攻击力
    public int def;//防御力

    public Soldier(String name, double hp, double atk, int def) {
        this.name = name;
        this.hp = hp;
        this.atk = atk;
        this.def = def;
    }

    public Soldier() {
    }

    public void attack(Soldier s) {
        double loseBloodWin = 0;//敌方英雄失血
        double loseBlood = 0;//我方英雄失血
        while (s.hp > 0 && this.hp > 0) {
            if (this.atk > s.def) {//敌方英雄失血
                loseBlood = this.atk - s.def;
            } else {
                loseBlood = s.def - this.atk;
            }
            if (this.atk < s.def) {//我方英雄失血
                loseBloodWin = s.def - this.atk;
            } else {
                loseBloodWin = this.atk - s.def;
            }
            s.hp -= loseBlood;//敌方剩余血量
            this.hp -= loseBloodWin;//我方剩余血量
            if (s.hp <= 0 && this.hp > 0) {
                System.out.println("战斗结束！" + this.name + "击败了" + s.name);
                break;
            } else if (s.hp > 0 && this.hp <= 0) {
                System.out.println("战斗结束！" + this.name + "已被击败！");
                break;
            }
            System.out.println(s.name + "剩余血量" + s.hp);
            System.out.println(this.name + "剩余血量" + this.hp);
        }
    }

    public void attack(Soldier s, Weapon weapon) {
        double loseBloodWin = 0;//敌方英雄失血
        double loseBlood = 0;//我方英雄失血
        while (s.hp > 0 && this.hp > 0) {
            if (this.atk + weapon.atk > s.def) {//敌方英雄失血
                loseBlood = this.atk + weapon.atk - s.def;
            } else {
                loseBlood = s.def - (this.atk + weapon.atk);
            }
            if (this.atk + weapon.atk < s.def) {//我方英雄失血
                loseBloodWin = s.def - (this.atk + weapon.atk);
            } else {
                loseBloodWin = this.atk + weapon.atk - s.def;
            }
            s.hp -= loseBlood;//敌方剩余血量
            this.hp -= loseBloodWin;//我方剩余血量
            if (s.hp <= 0 && this.hp > 0) {
                System.out.println("战斗结束！" + this.name + "使用" + weapon.name + "击败了" + s.name);
                break;
            } else if (s.hp > 0 && this.hp <= 0) {
                System.out.println("战斗结束！" + this.name + "已被击败！");
                break;
            }
            System.out.println(s.name + "剩余血量" + s.hp);
            System.out.println(this.name + "剩余血量" + this.hp);
        }
    }
}
