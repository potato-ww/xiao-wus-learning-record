package classes;

public class Dog {
    public String name;
    public int age;
    public String kind;

    public void play() {
        System.out.print("他喜欢吃狗粮,遛弯，游泳。");
    }

    public Dog(String name) {
        this.name = name;
    }

    public Dog(String name, int age) {
        this.name = name;
        this.age = age;
    }
}
