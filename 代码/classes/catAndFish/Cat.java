package classes.catAndFish;

public class Cat {
    public String name;

    //has-a
    /*public Fish f;

    public Cat(String name, Fish f) {
        this.name = name;
        this.f = f;
    }

    public void eat() {
        System.out.println(name + "爱吃" + f.kind);
    }*/
    //use-a
    public Cat(String name) {
        this.name = name;
    }

    public void eat(Fish f) {
        System.out.println(this.name + "爱吃" + f.kind);
    }
}
