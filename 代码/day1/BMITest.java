package day1;

import java.util.Scanner;

public class BMITest {
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        BMI();
    }

    public static void BMI() {
        System.out.println("请输入您的身高，体重：");
        double height = sc.nextDouble();
        double weight = sc.nextDouble();
        double bmi = weight / (height * height);
        if (bmi >= 35) {
            System.out.println("重度肥胖");
        } else if (bmi >= 30) {
            System.out.println("中度肥胖");
        } else if (bmi >= 27) {
            System.out.println("轻度肥胖");
        } else if (bmi >= 24) {
            System.out.println("体重过重");
        } else if (bmi >= 18.5) {
            System.out.println("正常范围");
        } else {
            System.out.println("体重过轻");
        }
    }
}
