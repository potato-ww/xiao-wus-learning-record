package day1;

import java.util.Scanner;

public class AtmF {
    /*【String[] userArr = {"zhangsan", "lisi"};
    String[] passwordArr = {"123", "456"};
    double[] accountArr = {500, 1000};
】李四的登录、存取查退*/
    static Scanner sc = new Scanner(System.in);


    public static void main(String[] args) {
        String[] userArr = {"zhangsan", "lisi"};
        String[] passwordArr = {"123", "456"};
        double[] accountArr = {500, 1000};
        int num = login(userArr, passwordArr);//获取下标
        while (num >= 0) {
            System.out.println("请选择您的操作：1、存钱 2、取钱 3、查询 4、退出");
            int handle = sc.nextInt();
            switch (handle) {
                case 1:
                    addMoneys(num, accountArr);
                    break;
                case 2:
                    lostMoney(num, accountArr);
                    break;
                case 3:
                    System.out.println("您的余额：" + accountArr[num]);
                    break;
                case 4:
                    System.out.println("退出成功");
                    num = -1;//退出操作Atm
                    break;
                default:
                    System.out.println("错误操作！");
                    break;
            }

        }
        if (num == -1) {
            System.out.println("查无此人");
        }
    }

    public static int login(String[] userArr, String[] passwordArr) {
        boolean login = true;//判断登录情况是否有效
        boolean flg = true;//判断用户是否输入有误
        int num = 5;
        while (num > 0) {
            System.out.println("请输入您的用户名和密码");
            String username = sc.next();
            String password = sc.next();
            for (int i = 0; i < userArr.length; i++) {
                if (userArr[i].equals(username) && passwordArr[i].equals(password)) {
                    System.out.println("登录成功");
                    flg = false;
                    return i;
                }
            }
            if (flg) {
                num--;
                if (num == 0) {
                    break;
                }
                System.out.println("输入有误！你还有" + num + "次机会！");
            }
        }
        return -1;
    }


    public static void addMoneys(int num, double[] accountArr) {
        System.out.println("请输入您要存入的金额：");
        double addMoney = sc.nextDouble();
        accountArr[num] += addMoney;
        System.out.println("您的余额：" + accountArr[num]);
    }

    public static void lostMoney(int num, double[] accountArr) {
        System.out.println("请输入您要取出的金额：");
        double lossMoney = sc.nextDouble();
        if (lossMoney <= accountArr[num]) {
            accountArr[num] -= lossMoney;
            System.out.println("取款成功！您的余额：" + accountArr[num]);
        } else {
            System.out.println("余额不足");
        }
    }

}
