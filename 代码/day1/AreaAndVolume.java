package day1;

import java.util.Scanner;

public class AreaAndVolume {
    /*根据输入的值 r 或 r 和 h，计算1. 圆的面积 PI*r*2，或者是2. 圆柱体的体积 v=PI*h*r*r*/
    static Scanner sc = new Scanner(System.in);

    /*public static void main(String[] args) {
        System.out.println("请输入r,h的值，计算圆的面积或者圆柱的体积：");
        double r = sc.nextDouble();
        double h = sc.nextDouble();
        aAndV(r);
        aAndV(r, h);
    }

    public static void aAndV(double r) {
        double area = 3.14 * r * r;
        System.out.println("圆的面积" + area);
    }

    public static void aAndV(double r, double h) {
        double volume = 3.14 * r * r * h;
        System.out.println("圆柱的体积" + volume);
    }*/
    /*比如 random(1,10) 可以得到1-10之间的随机数，random(20，30) 可以得到20-30的随机数，如果  random(5) 就可以得到 0-5 之间的随机数*/
    public static void main(String[] args) {
        random(5);
        random(18, 20);
    }

    public static void random(int a) {
        int b = 0;
        int num = (int) (Math.random() * (a - b + 1) + b);
        System.out.println(num);
    }

    public static void random(int a, int b) {
        int num = (int) (Math.random() * (b - a + 1) + a);
        System.out.println(num);
    }
}
