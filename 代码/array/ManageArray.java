package array;

import java.util.Scanner;

public class ManageArray {
    /* 【封装一个函数，实现传入一个整数数组，通过传 boolean 值，返回升序或降序数组】*/
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("请输入5个数字");
        int[] arr = new int[5];//输入数组
        for (int i = 0; i < 5; i++) {
            arr[i] = sc.nextInt();
        }
        System.out.println("请问你想升序排序：true或者降序排序:false");
        boolean fig = sc.nextBoolean();//判断升序降序
        ascendingOrDescending(fig, arr);//接收返回的数组
    }

    public static void ascendingOrDescending(boolean fig, int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            for (int j = i + 1; j < arr.length; j++) {
                if (fig) {//升序
                    if (arr[i] > arr[j]) {
                        int num = arr[i];
                        arr[i] = arr[j];
                        arr[j] = num;
                    }
                } else {//降序
                    if (arr[i] < arr[j]) {
                        int num = arr[i];
                        arr[i] = arr[j];
                        arr[j] = num;
                    }
                }
            }
        }
        for (int item : arr) {
            System.out.print(item + " ");
        }
    }
}
