package array;

public class ArrayX {
    public static void main(String[] args) {
        /*        *//*一维数组*//*
        int[] arr = new int[100];
        int[] arr2 = {1, 2, 3, 4, 5};
        *//*将100个数字存放到数组中*//*
        for (int i = 1; i <= 100; i++) {
            arr[i - 1] = i;
            System.out.println(arr[i - 1]);
        }*/


        /* 多维数组*/
   /*     int[][] allArr = new int[4][5];
        int k = 0;
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 5; j++) {
                allArr[i][j] = k;
                k++;
                System.out.print(allArr[i][j] + " ");
            }
            System.out.println("");
        }*/
        /*
         */
        /*已知5个学⽣的学号和3⻔课的成绩，求每个学⽣的平均成绩。输出所有学⽣的学号、3 ⻔课的成绩和平均成绩。*//*

        Scanner sc = new Scanner(System.in);
        int[][] cj = new int[5][5];
        int sum = 0;//总成绩
        double age = 0;//平均成绩
        for (int i = 0; i < 5; i++) {
            System.out.println("请输入第" + (i + 1) + "位同学的学号：");
            cj[i][0] = sc.nextInt();//学号
            for (int j = 1; j < 5; j++) {
                if (j == 4) {
                    cj[i][j] = sum;//第4位为总成绩
                    sum = 0;//把总成绩清零
                    break;//结束循环
                }
                System.out.println("请输入第" + (i + 1) + "位同学的" + (j) + "门课程的成绩：");
                cj[i][j] = sc.nextInt();//记录成绩
                sum += cj[i][j];//成绩相加
            }
        }
        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 5; j++) {
                if (j == 0) {//输出学号
                    System.out.print("学号：" + cj[i][0] + " ");
                } else if (j == 4) {//输出平均成绩
                    age = (double) cj[i][j] / 5;
                    System.out.print("平均分：" + age + " ");
                } else {//输出成绩
                    System.out.print("成绩" + j + "：" + cj[i][j] + " ");
                }
            }
            System.out.println(" ");
        }
*/
        /*已知5位同学的考试成绩，请找出最⾼分、最低分。*/
//        double[] cj = {60, 99, 80.5, 77, 34};
   /*     Scanner sc = new Scanner(System.in);
        double[] cj = new double[5];
        System.out.println("请输入五位同学的成绩：");
        double max = 0;//最高分
        for (int i = 0; i < cj.length; i++) {
            cj[i] = sc.nextDouble();
            if (cj[i] > max) {
                max = cj[i];
            }
        }
        System.out.println("最高分为" + max);*/
/*【String[] users = {"zhangsan", "lisi"};
        String[] passwords = {"123", "456"};
】实现登录，张三登录就欢迎张三，李四登录就欢迎李四*/
       /* String[] users = {"zhangsan", "lisi"};
        String[] passwords = {"123", "456"};
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入您的用户名和密码");
        String username = sc.next();
        String password = sc.next();
        boolean flg = true;
        for (int i = 0; i < users.length; i++) {
            if (users[i].equals(username) && passwords[i].equals(password)) {
                System.out.println(users[i] + "欢迎您！");
                flg = false;
                break;
            }
        }
        if (flg) {
            System.out.println("输入有误");
        }*/
       /* int[] arr1 = {20, 30, 40};
        int[] arr2 = {20, 30, 40};
        arr1[0] = 6;
        for (int i = 0; i < arr1.length; i++) {
            System.out.print(arr1[i] + " ");
        }
        System.out.println();
        for (int i = 0; i < arr2.length; i++) {
            System.out.print(arr2[i] + " ");
        }*/
       /*【int[] arr1 = {20, 30, 40};
        int[] arr2 = {50,60};
】需要得到一个数组里有 20, 30, 40，50，60*/
        /*int[] arr1 = {20, 30, 40};
        int[] arr2 = {50, 60};
        int[] arr3 = new int[arr1.length + arr2.length];
        int j = 0;//把2赋给3
        for (int i = 0; i < arr1.length + arr2.length; i++) {
            if (i < arr1.length) {
                arr3[i] = arr1[i];
            } else {
                arr3[i] = arr2[j];
                j++;
            }
        }
        for (int i = 0; i < arr3.length; i++) {
            System.out.print(arr3[i] + " ");
        }*/
        /* *//* 数组版ATM*//*
        String[] users = {"zhangsan", "lisi"};
        String[] passwords = {"123", "456"};
        int[] money = {500, 1000};
        int num = 0;//下标
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入您的用户名和密码");
        String username = sc.next();
        String password = sc.next();
        boolean flg = true;//判断登录是否有效
        boolean brIn = false;//判断登录是否成功
        for (int i = 0; i < users.length; i++) {
            if (users[i].equals(username) && passwords[i].equals(password)) {
                System.out.println(users[i] + "欢迎您！");
                flg = false;//登录有效
                brIn = true;//操作atm
                num = i;
                break;
            }
        }
        if (flg) {
            System.out.println("输入有误");
        }
        while (brIn) {
            System.out.println("请选择您的操作：1、存钱 2、取钱 3、查询 4、退出");
            int handle = sc.nextInt();
            switch (handle) {
                case 1:
                    System.out.println("请输入您要存入的金额：");
                    double addMoney = sc.nextDouble();
                    money[num] += addMoney;
                    System.out.println("您的余额：" + money[num]);
                    break;
                case 2:
                    System.out.println("请输入您要取出的金额：");
                    double lossMoney = sc.nextDouble();
                    if (lossMoney <= money[num]) {
                        money[num] -= lossMoney;
                        System.out.println("取款成功！您的余额：" + money[num]);
                    } else {
                        System.out.println("余额不足");
                    }
                    break;
                case 3:
                    System.out.println("您的余额：" + money[num]);
                    break;
                case 4:
                    System.out.println("退出成功");
                    brIn = false;//退出操作Atm
                    break;
                default:
                    System.out.println("错误操作！");
                    break;
            }

        }*/
        /*int[] arr = {12, 32, 15, 17, 2};
        for (int i = 0; i < arr.length; i++) {
            for (int j = i + 1; j < arr.length; j++) {
                if (arr[i] > arr[j]) {
                    int num = arr[i];
                    arr[i] = arr[j];
                    arr[j] = num;
                }
            }
        }
        for (int item : arr) {
            System.out.print(item + " ");
        }*/
       /* Scanner sc = new Scanner(System.in);
        int[] arr = new int[5];
        System.out.println("请输入五位同学的成绩：");
        for (int i = 0; i < arr.length; i++) {
            arr[i] = sc.nextInt();
        }
        for (int i = 0; i < arr.length; i++) {
            for (int j = i + 1; j < arr.length; j++) {
                if (arr[i] > arr[j]) {
                    int num = arr[i];
                    arr[i] = arr[j];
                    arr[j] = num;
                }
            }
        }
        for (int item : arr) {
            System.out.print(item + " ");
        }*/
/*        int[] arr = new int[6];
        for (int i = 0; i < arr.length; i++) {
            boolean br = false;
            int num = (int) (Math.random() * 10 + 1);
            for (int j = 0; j < i; j++) {
                if (num == arr[j]) {
                    br = true;
                }
            }
            if (br) {
                i--;
            } else {
                arr[i] = num;
            }
        }
        for (int item : arr) {
            System.out.print(item + " ");
        }*/
/*//字符串转数组,找出a的个数
        String str = "wbnmzaxc";
        char[] arr = str.toCharArray();
        int count = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == 'a') {
                count++;
            }
        }
        System.out.println(count);*/
//判断连续ab的个数
        String str = "abbbabcdab";
        char[] arr = str.toCharArray();
        int count = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == 'a' && arr[i + 1] == 'b') {
                count++;
            }
        }
        System.out.println(count);
    }
}
