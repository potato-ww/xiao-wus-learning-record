package array;

public class AddNum {
    public static void main(String[] args) {
        /*在数组 [1, 5, 8, 9] 里插入一个数字，在对应的位置。比如 [1, 5, 8, 9]  里加入 6，是不是就是  [1, 5, 6, 8, 9] 了？*/
        int[] arr = {1, 5, 8, 9};
        int num = 6;
        add(num, arr);
    }

    public static void add(int num, int[] arr) {
        int[] arrAdd = new int[arr.length + 1];
        for (int i = 0; i < arr.length; i++) {//把传入数组给新数组
            arrAdd[i] = arr[i];
        }
        arrAdd[arrAdd.length - 1] = num;//把加的数字存入数组
        for (int i = 0; i < arrAdd.length; i++) {//排序
            for (int j = i + 1; j < arrAdd.length; j++) {
                int num2 = 0;
                if (arrAdd[i] > arrAdd[j]) {
                    num2 = arrAdd[i];
                    arrAdd[i] = arrAdd[j];
                    arrAdd[j] = num2;
                }
            }
        }
        for (int item : arrAdd) {
            System.out.println(item);
        }

    }
}
