package array;

import java.util.Scanner;

public class SetX {
    /*   public static void main(String[] args) {
           sayHello("英⽂", "vv", 3, 4, 5);
           sayHello("中⽂", "维维");
       }

       public static void sayHello(String language, String userName,
                                   int... args) {
           switch (language) {
               case "英⽂":
                   System.out.println("Hello， " + userName);
                   break;
               case "中⽂":
                   System.out.println("你好，" + userName);
                   break;
           }
           if (args.length != 0) {
               for (int i = 0; i < args.length; i++) {
                   System.out.println(args[i]);
               }

               *//*for (int element : args) {
                System.out.println(element);
            }*//*
        }
    }*/
   /* ⽤递归编写⽅法，完成如下计算： 天朝有⼀个乞丐姓洪，去天桥要钱 第⼀天要了1块钱 第⼆天要了2块钱 第三天要了4块钱 第四天要了8块钱 以此类推。
    问题： 洪乞丐⼲10天，总收⼊是多少？*/
    public static int addMoney(int day) {
        int sum = 0;
        for (int i = 1; i <= day; i++) {
            sum += i;
        }
        return sum;
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入天数：");
        int day = sc.nextInt();
        System.out.println(addMoney(day));
    }
}
