package polymorphic.array;

import java.util.Arrays;

public class Array {
    //public int num;
    public Object[] arr;
    ;

    public Array() {
        arr = new Object[0];
    }

    public Array(String... rest) {
        arr = rest;
    }

    public void push(String s) {
        Object[] arrPush = new Object[arr.length + 1];//创建数组
        for (int i = 0; i < arr.length; i++) {
            arrPush[i] = arr[i];//把开始的值存入数组
        }
        arrPush[arr.length] = s;//把添加的值存入数组
        System.out.println("添加成功：");
        arr = arrPush;
    }

    public boolean remove(int underNum) {
        int num = arr.length - 1;
        if (arr.length - 1 >= 0) {
            Object[] arrRemove = new Object[arr.length - 1];//给中间数组填值
            if (underNum < arr.length && underNum >= 0) {//输入的下标不能大与数组的长度
                for (int i = 0; i < arrRemove.length; i++) {
                    if (i >= underNum && underNum >= 0) {//当下标与i相等以后就按这个里面计算，注意下标超界
                        arrRemove[i] = arr[i + 1];
                    } else {//i<传入下标的情况
                        arrRemove[i] = arr[i];
                    }

                }
                arr = arrRemove;
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    @Override
    public String toString() {
        return "Array{" +
                "arr=" + Arrays.toString(arr) +
                '}';
    }
}
