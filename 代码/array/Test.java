package polymorphic.array;

public class Test {
    public static void main(String[] args) {
        Array arr = new Array("v", "vv");
        System.out.println(arr);
        arr.push("vvv");
        System.out.println(arr);
        boolean fig = arr.remove(1);
        System.out.println(fig);
        System.out.println(arr);
        Array a = new Array();
        System.out.println(a);
        a.push("v");
        System.out.println(a);
        boolean fig1 = a.remove(0);
        System.out.println(fig1);
        System.out.println(a);
    }
}
