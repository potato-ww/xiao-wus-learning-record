package studentAtm;

import javax.swing.*;
import java.util.Date;

public class StudentAtm {
    //用户信息
    public static String[] record = new String[5];//交易记录
    public static String[] name = {"vv", "xx"};//用户名
    public static String[] password = {"123", "234"};//密码
    public static int[] money = {1000, 2000};//钱
    public static int[] lMoney = new int[2];//消费的钱
    public static int[] repay = new int[2];//返现的钱

    public static void main(String[] args) {
        int num = countLogin();
        //登录成功
        while (num >= 0) {
            int count = Integer.parseInt(JOptionPane.showInputDialog(null, "请选择：\n1、充值 \n2、刷卡消费 \n3、查询余额 \n4、查询交易记录\n5、修改密码\n6、安全退出"));
            //登录成功以后的操作
            switch (count) {
                case 1://充值
                    addMoney(num);
                    break;
                case 2://消费
                    lostMoney(num);
                    break;
                case 3://查询余额
                    JOptionPane.showMessageDialog(null, "您的余额为：" + money[num] + "\n您累积消费：" + lMoney[num]);
                    break;
                case 4://查询记录
                    recordLook();
                    break;
                case 5://修改密码
                    boolean fig = changePassword(num);
                    if (fig) {
                        num = login();
                    }
                    break;
                case 6:
                    num = countLogin();
                    break;
                default:
                    JOptionPane.showMessageDialog(null, "错误操作！！");
                    break;
            }
        }
    }

    //判断登录还是退出
    public static int countLogin() {
        int num = -1;
        int countLogin = Integer.parseInt(JOptionPane.showInputDialog(null, "请选择：\n1、登录系统 \n2、退出系统", "欢迎光临", 3));
        if (countLogin == 1) {
            num = login();
            return num;
        } else {
            JOptionPane.showMessageDialog(null, "谢谢使用请收好卡片");
            return num;
        }
    }

    //系统时间
    public static String getDate() {
        //系统时间
        Date date = new Date();
        int year = date.getYear() + 1900;
        int moth = date.getMonth() + 1;
        int day = date.getDay() + 25;
        return year + "-" + moth + "-" + day;
    }

    //登录
    public static int login() {
        int num = -1;//接收下标
        int count = 3;//计数
        while (count > 0) {
            String inpName = JOptionPane.showInputDialog(null, "请输入您的用户名：");
            String inpPassword = JOptionPane.showInputDialog(null, "请输入您的密码：");
            for (int i = 0; i < name.length; i++) {
                if (name[i].equals(inpName) && password[i].equals(inpPassword)) {
                    JOptionPane.showMessageDialog(null, "登录成功");
                    num = i;
                    return num;
                }
            }
            count--;
            if (count > 0) {
                JOptionPane.showMessageDialog(null, "登录失败,你还有" + count + "次机会！");
            } else {
                JOptionPane.showMessageDialog(null, "非法用户！！");
            }
        }
        return num;
    }

    //充值
    public static void addMoney(int num) {
        int addM = Integer.parseInt(JOptionPane.showInputDialog(null, "请输入充值金额："));
        money[num] += addM;//加上充值的钱
        String str = "充值金额：" + addM + "    " + getDate();
        for (int i = 0; i < record.length; i++) {
            if (record[i] == null) {
                record[i] = str;
                break;
            }
        }
        JOptionPane.showMessageDialog(null, "充值成功，您的余额为" + money[num]);
    }

    //消费
    public static void lostMoney(int num) {
        int lostM = Integer.parseInt(JOptionPane.showInputDialog(null, "请输入消费金额："));
        if (lostM <= money[num]) {
            lMoney[num] += lostM;//用的钱
            money[num] -= lostM;//消费的钱
            String str = "消费金额：" + lostM + "    " + getDate();
            for (int i = 0; i < record.length; i++) {
                if (record[i] == null) {
                    record[i] = str;
                    break;
                }
            }
            if (lMoney[num] >= 1000) {
                money[num] += 50 * (lMoney[num] / 1000);
                //判断有没有到返现条件
                repay[num] = 50 * (lMoney[num] / 1000);
                String strR = "返现金额：" + repay[num] + "    " + getDate();
                for (int i = 0; i < record.length; i++) {
                    if (record[i] == null) {
                        record[i] = strR;
                        break;
                    }
                }
            }
            JOptionPane.showMessageDialog(null, "消费成功\n您的余额为" + money[num] + "\n您的消费：" + lMoney[num]);
        } else {
            JOptionPane.showMessageDialog(null, "余额不足！！");
        }
    }

    //交易记录查询
    public static void recordLook() {
        String str = "交易记录              日期\n";
        for (int i = 0; i < record.length; i++) {
            if (record[i] != null) {
                str = str + record[i] + "\n";
            }
        }
        JOptionPane.showMessageDialog(null, str);
    }

    //修改密码
    public static boolean changePassword(int num) {
        String oldPass = JOptionPane.showInputDialog(null, "请输入旧密码：");
        if (oldPass.equals(password[num])) {
            String newPass = JOptionPane.showInputDialog(null, "请输入新密码：");
            String newPass2 = JOptionPane.showInputDialog(null, "请输入新密码：");
            if (newPass.equals(newPass2)) {
                password[num] = newPass;
                JOptionPane.showMessageDialog(null, "修改成功！请重新登录！");
                return true;
            } else {
                JOptionPane.showMessageDialog(null, "两次密码不一样");
            }
        } else {
            JOptionPane.showMessageDialog(null, "输入密码不正确！");
        }
        return false;
    }
}

